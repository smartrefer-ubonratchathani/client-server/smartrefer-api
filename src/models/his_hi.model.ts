import Knex = require('knex');
const hospcode = process.env.HIS_CODE;

export class HisHiModel {

    async getLogin(db: Knex, username: any, password: any) {
        let data = await db('dct')
        .select(`dct.dct as username`,db.raw(`CONCAT(dct.fname, ' ', dct.lname) as fullname`))
        .select(db('setup').first('hcode').as('hcode').limit(1))
        .where(`dct.dct` ,username)
        .andWhereRaw(`SUBSTRING(dct.cid, 10, 13) = ?`,[password]);
        return data;
    }

    async getServices_old(db: Knex, hn: any, seq: any, referno: any) {
        let data = await db.raw(`
        SELECT 
        o.vn as seq,p.pname as title_name,p.fname as first_name,p.lname as last_name, 
		DATE_FORMAT(date(o.vstdttm),'%Y-%m-%d') as date_serv, 
        DATE_FORMAT(time(o.vstdttm),'%h:%i:%s') as time_serv, 
        c.namecln as department
        FROM ovst as o 
        LEFT JOIN cln as c ON c.cln = o.cln 
        INNER JOIN pt as p	ON p.hn = o.hn
        WHERE o.hn ='${hn}' and o.vn = '${seq}' and o.an = '0'
		UNION
		SELECT 
		o.vn as seq,p.pname as title_name,p.fname as first_name,p.lname as last_name, 
		DATE_FORMAT(date(o.vstdttm),'%Y-%m-%d') as date_serv, 
        DATE_FORMAT(time(o.vstdttm),'%h:%i:%s') as time_serv, 
        idpm.nameidpm as department
		from ipt 
		LEFT JOIN idpm  on idpm.idpm = ipt.ward
		INNER JOIN ovst as o on o.vn = ipt.vn
	    INNER JOIN pt as p	ON p.hn = o.hn
        WHERE ipt.hn ='${hn}' and ipt.vn = '${seq}'
        `);
        return data[0];
    }

    async getServices(db: Knex, hn: any, seq: any, referno: any) {
        const query1 = db('ovst as o')
            .select(
                'o.vn as seq',
                'p.pname as title_name',
                'p.fname as first_name',
                'p.lname as last_name',
                db.raw(`DATE_FORMAT(o.vstdttm, '%Y-%m-%d') as date_serv`),
                db.raw(`DATE_FORMAT(o.vstdttm, '%h:%i:%s') as time_serv`),
                'c.namecln as department'
            )
            .leftJoin('cln as c', 'c.cln', 'o.cln')
            .innerJoin('pt as p', 'p.hn', 'o.hn')
            .where('o.hn', hn)
            .andWhere('o.vn', seq)
            .andWhere('o.an', '0');
    
        const query2 = db('ipt')
            .select(
                'o.vn as seq',
                'p.pname as title_name',
                'p.fname as first_name',
                'p.lname as last_name',
                db.raw(`DATE_FORMAT(o.vstdttm, '%Y-%m-%d') as date_serv`),
                db.raw(`DATE_FORMAT(o.vstdttm, '%h:%i:%s') as time_serv`),
                'idpm.nameidpm as department'
            )
            .leftJoin('idpm', 'idpm.idpm', 'ipt.ward')
            .innerJoin('ovst as o', 'o.vn', 'ipt.vn')
            .innerJoin('pt as p', 'p.hn', 'o.hn')
            .where('ipt.hn', hn)
            .andWhere('ipt.vn', seq);
    
        // ใช้ union พร้อม distinct
        const data = await query1.union(query2,true);
        return data;
    }

    async getProfile_old(db: Knex, hn: any, seq: any, referno: any) {
        let data = await db.raw(`
        select p.hn as hn, p.pop_id as cid, 
        if(p.pname = '',
        cast(
            (case p.male 
                when 1 then if(p.mrtlst < 6,
                    if(timestampdiff(YEAR,p.brthdate,now()) < 15,'ด.ช.','นาย'),
                    if((timestampdiff(YEAR,p.brthdate,now()) < 20),'เณร','พระ')) 
                when 2 then if((p.mrtlst = 1),
                    if((timestampdiff(YEAR,p.brthdate,now()) < 15),'ด.ญ.','น.ส.'),
                    if((p.mrtlst < 6),'นาง','แม่ชี')) 
            end) as char(8) charset utf8),
        convert(p.pname using utf8)) as title_name
        ,p.fname as first_name,p.lname as last_name
        ,p.moopart,p.addrpart,p.tmbpart,amppart,chwpart,p.brthdate
        ,concat(lpad(timestampdiff(year,p.brthdate,now()),3,'0'),'-'
        ,lpad(mod(timestampdiff(month,p.brthdate,now()),12),2,'0'),'-'
        ,lpad(if(DAYOFMONTH(p.brthdate)>DAYOFMONTH(now())
        ,day(LAST_DAY(SUBDATE(now(),INTERVAL 1 month)))-DAYOFMONTH(p.brthdate)+DAYOFMONTH(now())
        ,DAYOFMONTH(now())-DAYOFMONTH(p.brthdate)),2,'0')) as age
        ,if(p.male = 1,'ชาย','หญิง') as sex
        ,m.namemale as sexname,j.nameoccptn as occupation
        ,o.pttype as pttype_id,t.namepttype as pttype_name,s.card_id as pttype_no,s.hospmain
        ,c.hosname as hospmain_name,s.hospsub,h.hosname as hospsub_name,p.fdate as registdate,p.ldate as visitdate
        ,p.fthname as father_name,p.mthname as mother_name,p.couple as couple_name,p.infmname as contact_name,p.statusinfo as contact_relation,p.infmtel as contact_mobile
        FROM pt as p 
        left join ovst as o on o.vn = '${seq}'
        INNER JOIN pttype as t on p.pttype=t.pttype
        left join male as m on p.male=m.male
        left join occptn as j on p.occptn=j.occptn
        left join insure as s on p.hn=s.hn and p.pttype=s.pttype
        left join chospital as c on s.hospmain=c.hoscode
        left join chospital as h on s.hospmain=h.hoscode
        WHERE p.hn ='${hn}'
        `);
        return data[0];
    }

    async getProfile(db: Knex, hn: any, seq: any, referno: any) {
        const data = await db('pt as p')
            .select(
                'p.hn as hn',
                'p.pop_id as cid',
                db.raw(`
                    IF(
                        p.pname = '',
                        CAST(
                            CASE p.male
                                WHEN 1 THEN 
                                    IF(
                                        p.mrtlst < 6,
                                        IF(
                                            TIMESTAMPDIFF(YEAR, p.brthdate, NOW()) < 15, 'ด.ช.', 'นาย'
                                        ),
                                        IF(
                                            TIMESTAMPDIFF(YEAR, p.brthdate, NOW()) < 20, 'เณร', 'พระ'
                                        )
                                    )
                                WHEN 2 THEN 
                                    IF(
                                        p.mrtlst = 1,
                                        IF(
                                            TIMESTAMPDIFF(YEAR, p.brthdate, NOW()) < 15, 'ด.ญ.', 'น.ส.'
                                        ),
                                        IF(
                                            p.mrtlst < 6, 'นาง', 'แม่ชี'
                                        )
                                    )
                            END AS CHAR(8) CHARSET utf8
                        ),
                        CONVERT(p.pname USING utf8)
                    ) as title_name
                `),
                'p.fname as first_name',
                'p.lname as last_name',
                'p.moopart',
                'p.addrpart',
                'p.tmbpart',
                'p.amppart',
                'p.chwpart',
                'p.brthdate',
                db.raw(`
                    CONCAT(
                        LPAD(TIMESTAMPDIFF(YEAR, p.brthdate, NOW()), 3, '0'), '-',
                        LPAD(MOD(TIMESTAMPDIFF(MONTH, p.brthdate, NOW()), 12), 2, '0'), '-',
                        LPAD(
                            IF(
                                DAYOFMONTH(p.brthdate) > DAYOFMONTH(NOW()),
                                DAY(LAST_DAY(SUBDATE(NOW(), INTERVAL 1 MONTH))) - DAYOFMONTH(p.brthdate) + DAYOFMONTH(NOW()),
                                DAYOFMONTH(NOW()) - DAYOFMONTH(p.brthdate)
                            ), 2, '0'
                        )
                    ) as age
                `),
                db.raw(`IF(p.male = 1, 'ชาย', 'หญิง') as sex`),
                'm.namemale as sexname',
                'j.nameoccptn as occupation',
                'o.pttype as pttype_id',
                't.namepttype as pttype_name',
                's.card_id as pttype_no',
                's.hospmain',
                'c.hosname as hospmain_name',
                's.hospsub',
                'h.hosname as hospsub_name',
                'p.fdate as registdate',
                'p.ldate as visitdate',
                'p.fthname as father_name',
                'p.mthname as mother_name',
                'p.couple as couple_name',
                'p.infmname as contact_name',
                'p.statusinfo as contact_relation',
                'p.infmtel as contact_mobile'
            )
            .leftJoin('ovst as o', function () {
                this.on(db.raw('o.vn = ?',[seq])) })
            .innerJoin('pttype as t', 'p.pttype', 't.pttype')
            .leftJoin('male as m', 'p.male', 'm.male')
            .leftJoin('occptn as j', 'p.occptn', 'j.occptn')
            .leftJoin('insure as s', function () {
                this.on('p.hn', 's.hn').andOn('p.pttype', 's.pttype');
            })
            .leftJoin('chospital as c', 's.hospmain', 'c.hoscode')
            .leftJoin('chospital as h', 's.hospsub', 'h.hoscode')
            .where('p.hn', hn).limit(1);
    
        return data;
    }

    async getHospital_old(db: Knex, hn: any) {
        let data = await db.raw(`
        SELECT s.hcode as provider_code,h.namehosp as provider_name 
        FROM setup as s 
        INNER JOIN hospcode as h on h.off_id = s.hcode
        `);
        return data[0];
    }

    async getHospital(db: Knex, hn: any) {
        const data = await db('setup as s')
            .select(
                's.hcode as provider_code',
                'h.namehosp as provider_name'
            )
            .innerJoin('hospcode as h', 'h.off_id', 's.hcode');
    
        return data;
    }

    async getAllergyDetail_old(db: Knex, hn: any, referno: any) {
        let data = await db.raw(`
        SELECT namedrug as drug_name, replace(replace(detail,char(13),' '),char(10),' ') as symptom ,entrydate as begin_date,entrydate as daterecord
        FROM allergy 
        WHERE hn ='${hn}'
        `);
        return data[0];
    }

    async getAllergyDetail(db: Knex, hn: any, referno: any) {
        const data = await db('allergy')
            .select(
                'namedrug as drug_name',
                db.raw("REPLACE(REPLACE(detail, CHAR(13), ' '), CHAR(10), ' ') as symptom"),
                'entrydate as begin_date',
                'entrydate as daterecord'
            )
            .where('hn', hn);
    
        return data;
    }

    async getChronic_old(db: Knex, hn: any, referno: any) {
        let data = await db.raw(`
        SELECT c.chronic as icd_code, c.date_diag as start_date, IF(i.name_t!='', i.name_t, "-") as icd_name
        FROM chronic as c 
        INNER JOIN icd101 as i on i.icd10 = c.chronic
        WHERE c.pid ='${hn}'
        `);
        return data[0];
    }

    async getChronic(db: Knex, hn: any, referno: any) {
        const data = await db('chronic as c')
            .select(
                'c.chronic as icd_code',
                'c.date_diag as start_date',
                db.raw("IF(i.name_t != '', i.name_t, '-') as icd_name")
            )
            .innerJoin('icd101 as i', 'i.icd10', 'c.chronic')
            .where('c.pid', hn);
    
        return data;
    }

    async getDiagnosis_old(db: Knex, hn: any, dateServe: any, seq: any, referno: any) {
        let data = await db.raw(`
        SELECT o.vn as seq, date_format(ovst.vstdttm,'%Y-%m-%d') as date_serv, time(ovst.vstdttm) as time_serv
        , o.icd10 as icd_code,i.icd10name as icd_name,'1' as diag_type, d.diagtext as DiagNote,'1' as diagtype_id
        FROM orfro as o
        INNER JOIN ovst on ovst.vn = o.vn
        INNER JOIN icd101 as i on i.icd10 = o.icd10
        left JOIN visitdiagtext as d on o.vn = d.vn 
        WHERE o.vn ='${seq}'
        Union 
        SELECT o.vn as seq, date_format(ovst.vstdttm,'%Y-%m-%d') as date_serv, time(ovst.vstdttm) as time_serv
        , o.icd10 as icd_code,IF(o.icd10name!='', o.icd10name, i.icd10name) as icd_name
        , o.cnt as diag_type, d.diagtext as DiagNote, 
        '1' as diagtype_id
        FROM ovstdx as o
        INNER JOIN ovst on ovst.vn = o.vn
        INNER JOIN orfro on orfro.vn != o.vn
        INNER JOIN icd101 as i on i.icd10 = o.icd10
        left JOIN visitdiagtext as d on o.vn = d.vn 
        WHERE o.vn ='${seq}' and cnt = 1
        Union 
        SELECT o.vn as seq, date_format(ovst.vstdttm,'%Y-%m-%d') as date_serv, time(ovst.vstdttm) as time_serv
        , o.icd10 as icd_code,IF(o.icd10name!='', o.icd10name, i.icd10name) as icd_name
        , o.cnt as diag_type, d.diagtext as DiagNote, 
        (case 
            when o.icd10 not between 'V0000' and 'Y9999' then 4
            when o.icd10 between 'V0000' and 'Y9999' then 5 else 4 end)  as diagtype_id
        FROM ovstdx as o
        INNER JOIN ovst on ovst.vn = o.vn
        INNER JOIN icd101 as i on i.icd10 = o.icd10
        left JOIN visitdiagtext as d on o.vn = d.vn 
        WHERE o.vn ='${seq}' and ovst.an='0' and cnt <> 1
        UNION 
        SELECT 
        dt.vn as seq,
        date_format(dt.vstdttm,'%Y-%m-%d') as date_serv,
        time(dt.vstdttm) as time_serv,
        replace(x.icdda,'.','') as icd_code,
        c.icd10name as icd_name,
        '4' as diag_type,
        d.diagtext as DiagNote,
        '4' as diag_type_id
        from dt
        inner join dtdx as x 
        on dt.dn=x.dn
        left join icd101 as c on replace(x.icdda,'.','') = c.icd10
        left join visitdiagtext as d on dt.vn=d.vn
        where dt.vn='${seq}'
        UNION 
		SELECT ipt.vn as seq
		, date_format(ipt.rgtdate,'%Y-%m-%d') as date_serv
		, time(ipt.rgttime) as time_serv
        , o.icd10 as icd_code,i.icd10name as icd_name
        , o.itemno as diag_type
		, '' as DiagNote
		, (case 
            when o.icd10 not between 'V0000' and 'Y9999' then 4
            when o.icd10 between 'V0000' and 'Y9999' then 5 else 4 end)  as diagtype_id
        FROM iptdx as o
        INNER JOIN ipt on ipt.an = o.an and o.itemno <> 1
        INNER JOIN icd101 as i on i.icd10 = o.icd10 
        WHERE ipt.vn ='${seq}'
        `);
        return data[0];
    }
    async getDiagnosis_old2(db: Knex, hn: any, dateServe: any, seq: any, referno: any) {
        const data = await db.raw(`
            SELECT o.vn as seq, date_format(ovst.vstdttm,'%Y-%m-%d') as date_serv, time(ovst.vstdttm) as time_serv,
            o.icd10 as icd_code, i.icd10name as icd_name, '1' as diag_type, d.diagtext as DiagNote, '1' as diagtype_id
            FROM orfro as o
            INNER JOIN ovst on ovst.vn = o.vn
            INNER JOIN icd101 as i on i.icd10 = o.icd10
            LEFT JOIN visitdiagtext as d on o.vn = d.vn
            WHERE o.vn = ?
    
            UNION
    
            SELECT o.vn as seq, date_format(ovst.vstdttm,'%Y-%m-%d') as date_serv, time(ovst.vstdttm) as time_serv,
            o.icd10 as icd_code, IF(o.icd10name != '', o.icd10name, i.icd10name) as icd_name, 
            o.cnt as diag_type, d.diagtext as DiagNote, '1' as diagtype_id
            FROM ovstdx as o
            INNER JOIN ovst on ovst.vn = o.vn
            INNER JOIN orfro on orfro.vn != o.vn
            INNER JOIN icd101 as i on i.icd10 = o.icd10
            LEFT JOIN visitdiagtext as d on o.vn = d.vn
            WHERE o.vn = ? AND o.cnt = 1
    
            UNION
    
            SELECT o.vn as seq, date_format(ovst.vstdttm,'%Y-%m-%d') as date_serv, time(ovst.vstdttm) as time_serv,
            o.icd10 as icd_code, IF(o.icd10name != '', o.icd10name, i.icd10name) as icd_name,
            o.cnt as diag_type, d.diagtext as DiagNote,
            (CASE 
                WHEN o.icd10 NOT BETWEEN 'V0000' AND 'Y9999' THEN 4
                WHEN o.icd10 BETWEEN 'V0000' AND 'Y9999' THEN 5
                ELSE 4
            END) as diagtype_id
            FROM ovstdx as o
            INNER JOIN ovst on ovst.vn = o.vn
            INNER JOIN icd101 as i on i.icd10 = o.icd10
            LEFT JOIN visitdiagtext as d on o.vn = d.vn
            WHERE o.vn = ? AND ovst.an = '0' AND o.cnt <> 1
    
            UNION
    
            SELECT dt.vn as seq, date_format(dt.vstdttm,'%Y-%m-%d') as date_serv, time(dt.vstdttm) as time_serv,
            REPLACE(x.icdda, '.', '') as icd_code, c.icd10name as icd_name, '4' as diag_type, d.diagtext as DiagNote, '4' as diagtype_id
            FROM dt
            INNER JOIN dtdx as x on dt.dn = x.dn
            LEFT JOIN icd101 as c on REPLACE(x.icdda, '.', '') = c.icd10
            LEFT JOIN visitdiagtext as d on dt.vn = d.vn
            WHERE dt.vn = ?
    
            UNION
    
            SELECT ipt.vn as seq, date_format(ipt.rgtdate,'%Y-%m-%d') as date_serv, time(ipt.rgttime) as time_serv,
            o.icd10 as icd_code, i.icd10name as icd_name, o.itemno as diag_type, '' as DiagNote,
            (CASE 
                WHEN o.icd10 NOT BETWEEN 'V0000' AND 'Y9999' THEN 4
                WHEN o.icd10 BETWEEN 'V0000' AND 'Y9999' THEN 5
                ELSE 4
            END) as diagtype_id
            FROM iptdx as o
            INNER JOIN ipt on ipt.an = o.an AND o.itemno <> 1
            INNER JOIN icd101 as i on i.icd10 = o.icd10
            WHERE ipt.vn = ?
        `, [seq, seq, seq, seq, seq]);
    
        return data[0];
    }
    async getDiagnosis(db:Knex, hn:any, dateServe:any, seq:any, referno:any) {
        const query1 = db('orfro as o')
          .select(
            'o.vn as seq',
            db.raw("DATE_FORMAT(ovst.vstdttm,'%Y-%m-%d') as date_serv"),
            db.raw('TIME(ovst.vstdttm) as time_serv'),
            'o.icd10 as icd_code',
            'i.icd10name as icd_name',
            db.raw("'1' as diag_type"),
            'd.diagtext as DiagNote',
            db.raw("'1' as diagtype_id")
          )
          .innerJoin('ovst', 'ovst.vn', 'o.vn')
          .innerJoin('icd101 as i', 'i.icd10', 'o.icd10')
          .leftJoin('visitdiagtext as d', 'o.vn', 'd.vn')
          .where('o.vn', seq);
      
        const query2 = db('ovstdx as o')
          .select(
            'o.vn as seq',
            db.raw("DATE_FORMAT(ovst.vstdttm,'%Y-%m-%d') as date_serv"),
            db.raw('TIME(ovst.vstdttm) as time_serv'),
            'o.icd10 as icd_code',
            db.raw("IF(o.icd10name != '', o.icd10name, i.icd10name) as icd_name"),
            'o.cnt as diag_type',
            'd.diagtext as DiagNote',
            db.raw("'1' as diagtype_id")
          )
          .innerJoin('ovst', 'ovst.vn', 'o.vn')
          .innerJoin('orfro', function () {
            this.on('orfro.vn', '!=', 'o.vn');
          })
          .innerJoin('icd101 as i', 'i.icd10', 'o.icd10')
          .leftJoin('visitdiagtext as d', 'o.vn', 'd.vn')
          .where('o.vn', seq)
          .andWhere('o.cnt', 1);
      
        const query3 = db('ovstdx as o')
          .select(
            'o.vn as seq',
            db.raw("DATE_FORMAT(ovst.vstdttm,'%Y-%m-%d') as date_serv"),
            db.raw('TIME(ovst.vstdttm) as time_serv'),
            'o.icd10 as icd_code',
            db.raw("IF(o.icd10name != '', o.icd10name, i.icd10name) as icd_name"),
            'o.cnt as diag_type',
            'd.diagtext as DiagNote',
            db.raw(`
              (CASE 
                WHEN o.icd10 NOT BETWEEN 'V0000' AND 'Y9999' THEN 4
                WHEN o.icd10 BETWEEN 'V0000' AND 'Y9999' THEN 5
                ELSE 4
              END) as diagtype_id
            `)
          )
          .innerJoin('ovst', 'ovst.vn', 'o.vn')
          .innerJoin('icd101 as i', 'i.icd10', 'o.icd10')
          .leftJoin('visitdiagtext as d', 'o.vn', 'd.vn')
          .where('o.vn', seq)
          .andWhere('ovst.an', '0')
          .andWhere('o.cnt', '<>', 1);
      
        const query4 = db('dt')
          .select(
            'dt.vn as seq',
            db.raw("DATE_FORMAT(dt.vstdttm,'%Y-%m-%d') as date_serv"),
            db.raw('TIME(dt.vstdttm) as time_serv'),
            db.raw("REPLACE(x.icdda, '.', '') as icd_code"),
            'c.icd10name as icd_name',
            db.raw("'4' as diag_type"),
            'd.diagtext as DiagNote',
            db.raw("'4' as diagtype_id")
          )
          .innerJoin('dtdx as x', 'dt.dn', 'x.dn')
          .leftJoin('icd101 as c', function () {
                this.on(db.raw("REPLACE(x.icdda, '.', '') = c.icd10"));
              })

          .leftJoin('visitdiagtext as d', 'dt.vn', 'd.vn')
          .where('dt.vn', seq);
      
        const query5 = db('iptdx as o')
          .select(
            'ipt.vn as seq',
            db.raw("DATE_FORMAT(ipt.rgtdate,'%Y-%m-%d') as date_serv"),
            db.raw('TIME(ipt.rgttime) as time_serv'),
            'o.icd10 as icd_code',
            'i.icd10name as icd_name',
            'o.itemno as diag_type',
            db.raw("'' as DiagNote"),
            db.raw(`
              (CASE 
                WHEN o.icd10 NOT BETWEEN 'V0000' AND 'Y9999' THEN 4
                WHEN o.icd10 BETWEEN 'V0000' AND 'Y9999' THEN 5
                ELSE 4
              END) as diagtype_id
            `)
          )
          .innerJoin('ipt', function () {
            this.on('ipt.an', 'o.an').andOn(db.raw('o.itemno <> 1'));
          })
          .innerJoin('icd101 as i', 'i.icd10', 'o.icd10')
          .where('ipt.vn', seq);
      
        // รวม query ด้วย UNION
        const result = await db
          .union([query1, query2, query3, query4, query5], true) // true สำหรับ UNION ALL
          .select();
      
        return result;
      }
    async getRefer(db: Knex, hn: any, dateServe: any, seq: any, referno: any) {
        let data = await db.raw(`
    select o.vn as seq,o.an as an,ovst.hn as pid, ovst.hn as hn, 
    cast(replace(o.rfrno,"/",'-') as char(20) CHARACTER SET UTF8) as referno,
        o.dchdate as referdate, 
        o.hcode as to_hcode, 
        if(t.trauma='T',1,2) as pttype_id,
        if(t.trauma='T','Trauma','Non Trauma') as pttype_name,
        if(t.triage='' or t.triage is null,'5',t.triage) as strength_id,
        (case t.triage 
            when 1 then 'Resucitate'
            when 2 then 'Emergency'
            when 3 then 'Urgency'
            when 4 then 'Semi Urgency'
            when 5 then 'Non Urgency'
            else 'Non Urgency' end 
            ) as strength_name,
  if(ovst.cln='' or ovst.cln is null,'',c.namecln) as location_name,
  if(ovst.cln='' or ovst.cln is null,'IPD',if(substr(ovst.cln,1,1)=2,'ER','OPD')) as station_name,
  '' as loads_id, 
       '' as loads_name,
        h.namehosp as to_hcode_name, 
        '' as refer_cause,
     time(o.dchtime*100) as refertime,
     if(ovst.dct='' or ovst.dct is null,concat(dentist.lcno,',',dentist.cid),concat(d.lcno,',',d.cid))  as doctor,
     if(ovst.dct='' or ovst.dct is null,CONCAT(dentist.fname,' ',dentist.lname),CONCAT(d.fname,' ',d.lname))  as doctor_name
     ,replace(replace(concat(substr(referresul,instr(referresul,'CC'),instr(referresul,'PE')-instr(referresul,'CC')),
     substr(referresul,instr(referresul,'PrX')),ifnull(va.advice,''),ifnull(vc.detail,'')),char(13),'\n'),char(10),' ') as refer_remark
     from referresult as o
        left Join hospcode as h on h.off_id = o.hcode
        inner join ovst on ovst.vn = o.vn
        left outer join cln as c on ovst.cln=c.cln 
        left outer Join dct as d on (case length(ovst.dct) when 5 then d.lcno=ovst.dct when 4 then substr(ovst.dct,1,2) = d.dct end) 
        left outer Join dt on dt.vn=ovst.vn
        left outer Join dtdx on dtdx.dn=dt.dn
        left outer Join dentist on dentist.codedtt=dt.dnt    
        left outer join optriage as t on o.vn=t.vn
        left outer join visitadvice as va on o.vn=va.vn 
        left outer join visitnoteconsult as vc on o.vn=vc.vn        
        where o.vn  ='${seq}' and cast(replace(o.rfrno,"/",'-') as char(20) CHARACTER SET UTF8) = '${referno}'
    UNION
    select o.vn as seq,o.an as an,ovst.hn as pid, ovst.hn as hn, 
    cast(replace(o.rfrno,"/",'-') as char(20) CHARACTER SET UTF8) as referno,
        o.vstdate as referdate, 
        o.rfrlct as to_hcode, 
        if(t.trauma='T',1,2) as pttype_id,
        if(t.trauma='T','Trauma','Non Trauma') as pttype_name,
        if(t.triage='' or t.triage is null,'5',t.triage) as strength_id,
        (case t.triage 
            when 1 then 'Resucitate'
            when 2 then 'Emergency'
            when 3 then 'Urgency'
            when 4 then 'Semi Urgency'
            when 5 then 'Non Urgency'
            else 'Non Urgency' end 
            ) as strength_name,
        if(o.cln='' or o.cln is null,i.nameidpm,c.namecln) as location_name,
        if(o.cln='' or o.cln is null,'IPD',if(substr(o.cln,1,1)=2,'ER','OPD')) as station_name,
        if(o.loads='' or o.loads is null,'4',o.loads) as loads_id, 
        if(lo.nameloads='' or lo.nameloads is null,'ไปเอง',lo.nameloads) as loads_name,
        h.namehosp as to_hcode_name, 
        concat(ifnull(f.namerfrcs,''),' Consult : ',ifnull(sp.nametype,'')) as refer_cause,
        time(o.vsttime*100) as refertime,
	     if(ovst.cln!='40100',if(ovst.an='0',(if(ovst.dct='' or ovst.dct is null,concat(dentist.lcno,',',dentist.cid),concat(d.lcno,',',d.cid))),concat(iptdx.dct,',',dct.cid)),concat(dentist.lcno,',',dentist.cid))  as doctor,
        if(ovst.cln!='40100',if(ovst.dct='' or ovst.dct is null,CONCAT(dentist.fname,' ',dentist.lname),(if(ovst.an='0',CONCAT(d.fname,' ',d.lname),(CONCAT(dct.fname,' ',dct.lname))))),CONCAT(dentist.fname,' ',dentist.lname)) as doctor_name
        ,replace(replace(vc.detail,char(13),'\n'),char(10),' ') as refer_remark
        from orfro as o
        left Join hospcode as h on h.off_id = o.rfrlct
        inner join ovst on ovst.vn = o.vn
        left Join rfrcs as f on f.rfrcs = o.rfrcs
				left outer join cln as c on o.cln=c.cln 
				left outer join idpm as i on o.ward=i.idpm
        left outer Join dct as d on (case length(ovst.dct) when 5 then d.lcno=ovst.dct when 4 then substr(ovst.dct,1,2) = d.dct end)
        left outer Join dt on dt.vn=ovst.vn
        left outer Join dtdx on dtdx.dn=dt.dn        
				left outer Join ipt on ipt.vn=o.vn
				left outer Join iptdx on iptdx.an=ipt.an
				left outer Join dct on dct.lcno=iptdx.dct
        left outer Join dentist on dentist.codedtt=dt.dnt    
        left outer join optriage as t on o.vn=t.vn
        left outer join l_loads as lo on o.loads = lo.codeloads
        left outer join l_rfrtype as sp on o.rfrtype=sp.rfrtype 
        left join visitnoteconsult as vc on o.vn=vc.vn
        where o.vn  ='${seq}' and cast(replace(o.rfrno,"/",'-') as char(20) CHARACTER SET UTF8) = '${referno}' limit 1
        `);
        return data[0];
    }

    async getRefer_xx(db: Knex, hn: any, dateServe: any, seq: any, referno: any) {
        const query1 = db('referresult as o')
            .select([
                'o.vn as seq',
                'o.an as an',
                'ovst.hn as pid',
                'ovst.hn as hn',
                db.raw(`CAST(REPLACE(o.rfrno, "/", "-") AS CHAR(20) CHARACTER SET UTF8) AS referno`),
                'o.dchdate as referdate',
                'o.hcode as to_hcode',
                db.raw(`IF(t.trauma = 'T', 1, 2) AS pttype_id`),
                db.raw(`IF(t.trauma = 'T', 'Trauma', 'Non Trauma') AS pttype_name`),
                db.raw(`IF(t.triage = '' OR t.triage IS NULL, '5', t.triage) AS strength_id`),
                db.raw(`
                    CASE t.triage
                        WHEN 1 THEN 'Resucitate'
                        WHEN 2 THEN 'Emergency'
                        WHEN 3 THEN 'Urgency'
                        WHEN 4 THEN 'Semi Urgency'
                        WHEN 5 THEN 'Non Urgency'
                        ELSE 'Non Urgency'
                    END AS strength_name
                `),
                db.raw(`IF(ovst.cln = '' OR ovst.cln IS NULL, '', c.namecln) AS location_name`),
                db.raw(`IF(ovst.cln = '' OR ovst.cln IS NULL, 'IPD', IF(SUBSTR(ovst.cln, 1, 1) = 2, 'ER', 'OPD')) AS station_name`),
                db.raw(`'' AS loads_id`),
                db.raw(`'' AS loads_name`),
                'h.namehosp as to_hcode_name',
                db.raw(`'' AS refer_cause`),
                db.raw(`TIME(o.dchtime * 100) AS refertime`),
                db.raw(`
                    IF(ovst.dct = '' OR ovst.dct IS NULL, 
                        CONCAT(dentist.lcno, ',', dentist.cid), 
                        CONCAT(d.lcno, ',', d.cid)
                    ) AS doctor
                `),
                db.raw(`
                    IF(ovst.dct = '' OR ovst.dct IS NULL, 
                        CONCAT(dentist.fname, ' ', dentist.lname), 
                        CONCAT(d.fname, ' ', d.lname)
                    ) AS doctor_name
                `),
                db.raw(`
                    REPLACE(REPLACE(CONCAT(
                        SUBSTR(referresul, INSTR(referresul, 'CC'), INSTR(referresul, 'PE') - INSTR(referresul, 'CC')),
                        SUBSTR(referresul, INSTR(referresul, 'PrX')),
                        IFNULL(va.advice, ''),
                        IFNULL(vc.detail, '')
                    ), CHAR(13), '\n'), CHAR(10), ' ') AS refer_remark
                `),
            ])
            .leftJoin('hospcode as h', 'h.off_id', 'o.hcode')
            .innerJoin('ovst', 'ovst.vn', 'o.vn')
            .leftJoin('cln as c', 'ovst.cln', 'c.cln')
            .leftJoin('dct as d', db.raw(`
                CASE LENGTH(ovst.dct)
                    WHEN 5 THEN d.lcno = ovst.dct
                    WHEN 4 THEN SUBSTR(ovst.dct, 1, 2) = d.dct
                END
            `))
            .leftJoin('dt', 'dt.vn', 'ovst.vn')
            .leftJoin('dtdx', 'dtdx.dn', 'dt.dn')
            .leftJoin('dentist', 'dentist.codedtt', 'dt.dnt')
            .leftJoin('optriage as t', 'o.vn', 't.vn')
            .leftJoin('visitadvice as va', 'o.vn', 'va.vn')
            .leftJoin('visitnoteconsult as vc', 'o.vn', 'vc.vn')
            .where('o.vn', seq)
            .andWhereRaw(`CAST(REPLACE(o.rfrno, "/", "-") AS CHAR(20) CHARACTER SET UTF8) = ?`, [referno]);
    
        const query2 = db('orfro as o')
            .select([
                'o.vn as seq',
                'o.an as an',
                'ovst.hn as pid',
                'ovst.hn as hn',
                db.raw(`CAST(REPLACE(o.rfrno, "/", "-") AS CHAR(20) CHARACTER SET UTF8) AS referno`),
                'o.vstdate as referdate',
                'o.rfrlct as to_hcode',
                db.raw(`IF(t.trauma = 'T', 1, 2) AS pttype_id`),
                db.raw(`IF(t.trauma = 'T', 'Trauma', 'Non Trauma') AS pttype_name`),
                db.raw(`IF(t.triage = '' OR t.triage IS NULL, '5', t.triage) AS strength_id`),
                db.raw(`
                    CASE t.triage
                        WHEN 1 THEN 'Resucitate'
                        WHEN 2 THEN 'Emergency'
                        WHEN 3 THEN 'Urgency'
                        WHEN 4 THEN 'Semi Urgency'
                        WHEN 5 THEN 'Non Urgency'
                        ELSE 'Non Urgency'
                    END AS strength_name
                `),
                db.raw(`IF(o.cln = '' OR o.cln IS NULL, i.nameidpm, c.namecln) AS location_name`),
                db.raw(`IF(o.cln = '' OR o.cln IS NULL, 'IPD', IF(SUBSTR(o.cln, 1, 1) = 2, 'ER', 'OPD')) AS station_name`),
                db.raw(`IF(o.loads = '' OR o.loads IS NULL, '4', o.loads) AS loads_id`),
                db.raw(`IF(lo.nameloads = '' OR lo.nameloads IS NULL, 'ไปเอง', lo.nameloads) AS loads_name`),
                'h.namehosp as to_hcode_name',
                db.raw(`CONCAT(IFNULL(f.namerfrcs, ''), ' Consult : ', IFNULL(sp.nametype, '')) AS refer_cause`),
                db.raw(`TIME(o.vsttime * 100) AS refertime`),
                db.raw(`
                    IF(ovst.cln != '40100', 
                        IF(ovst.an = '0', 
                            IF(ovst.dct = '' OR ovst.dct IS NULL, 
                                CONCAT(dentist.lcno, ',', dentist.cid), 
                                CONCAT(d.lcno, ',', d.cid)
                            ), 
                            CONCAT(iptdx.dct, ',', dct.cid)
                        ), 
                        CONCAT(dentist.lcno, ',', dentist.cid)
                    ) AS doctor
                `),
                db.raw(`
                    IF(ovst.cln != '40100', 
                        IF(ovst.dct = '' OR ovst.dct IS NULL, 
                            CONCAT(dentist.fname, ' ', dentist.lname), 
                            IF(ovst.an = '0', 
                                CONCAT(d.fname, ' ', d.lname), 
                                CONCAT(dct.fname, ' ', dct.lname)
                            )
                        ), 
                        CONCAT(dentist.fname, ' ', dentist.lname)
                    ) AS doctor_name
                `),
                db.raw(`REPLACE(REPLACE(vc.detail, CHAR(13), '\n'), CHAR(10), ' ') AS refer_remark`),
            ])
            .leftJoin('hospcode as h', 'h.off_id', 'o.rfrlct')
            .innerJoin('ovst', 'ovst.vn', 'o.vn')
            .leftJoin('rfrcs as f', 'f.rfrcs', 'o.rfrcs')
            .leftJoin('cln as c', 'o.cln', 'c.cln')
            .leftJoin('idpm as i', 'o.ward', 'i.idpm')
            .leftJoin('dct as d', db.raw(`
                CASE LENGTH(ovst.dct)
                    WHEN 5 THEN d.lcno = ovst.dct
                    WHEN 4 THEN SUBSTR(ovst.dct, 1, 2) = d.dct
                END
            `))
            .leftJoin('dt', 'dt.vn', 'ovst.vn')
            .leftJoin('dtdx', 'dtdx.dn', 'dt.dn')
            .leftJoin('ipt', 'ipt.vn', 'o.vn')
            .leftJoin('iptdx', 'iptdx.an', 'ipt.an')
            .leftJoin('dct', 'dct.lcno', 'iptdx.dct')
            .leftJoin('dentist', 'dentist.codedtt', 'dt.dnt')
            .leftJoin('optriage as t', 'o.vn', 't.vn')
            .leftJoin('l_loads as lo', 'o.loads', 'lo.codeloads')
            .leftJoin('l_rfrtype as sp', 'o.rfrtype', 'sp.rfrtype')
            .leftJoin('visitnoteconsult as vc', 'o.vn', 'vc.vn')
            .where('o.vn', seq)
            .andWhereRaw(`CAST(REPLACE(o.rfrno, "/", "-") AS CHAR(20) CHARACTER SET UTF8) = ?`, [referno]);
    
        // const data = await query1.union(query2).limit(1);
        const result = await db
        .union([query1, query2], true) // true สำหรับ UNION ALL
        .select();
    
      return result;
        // return data;
    }

    async getDrugs_old(db: Knex, hn: any, dateServe: any, seq: any, referno: any) {
        let data = await db.raw(`
        select p.vn as seq,
        DATE_FORMAT(date(p.prscdate),'%Y%m%d') as date_serv,
        DATE_FORMAT(time(p.prsctime),'%h:%i:%s') as time_serv, 
        pd.nameprscdt as drug_name,
        pd.qty as qty, 
        med.pres_unt as unit ,
        if(pd.medusage = '',x.doseprn,IF(m.doseprn1!='', m.doseprn1, '')) as usage_line1 ,
        IF(m.doseprn2!='', m.doseprn2, '') as usage_line2,
        '' as usage_line3
        FROM prsc as p 
        left Join prscdt as pd ON pd.PRSCNO = p.PRSCNO 
        left Join medusage as m ON m.dosecode = pd.medusage
        left join xdose as x ON pd.xdoseno=x.xdoseno
        left Join meditem as med ON med.meditem = pd.meditem  
        WHERE p.vn = '${seq}' and med.type in (1,3,5) order by date_serv DESC
        `);
        return data[0];
    }

    async getDrugs(db: Knex, hn: any, dateServe: any, seq: any, referno: any) {
        const data = await db('prsc as p')
            .select(
                'p.vn as seq',
                db.raw(`MIN(DATE_FORMAT(p.prscdate, '%Y%m%d')) as date_serv`),
                db.raw(`DATE_FORMAT(p.prsctime, '%h:%i:%s') as time_serv`),
                'pd.nameprscdt as drug_name',
                db.raw('sum(pd.qty) as qty'),
                'med.pres_unt as unit',
                db.raw(`
                    IF(pd.medusage = '', x.doseprn, IF(m.doseprn1 != '', m.doseprn1, '')) as usage_line1
                `),
                db.raw(`
                    IF(m.doseprn2 != '', m.doseprn2, '') as usage_line2
                `),
                db.raw(`'' as usage_line3`)
            )
            .leftJoin('prscdt as pd', 'pd.PRSCNO', 'p.PRSCNO')
            .leftJoin('medusage as m', 'm.dosecode', 'pd.medusage')
            .leftJoin('xdose as x', 'pd.xdoseno', 'x.xdoseno')
            .leftJoin('meditem as med', 'med.meditem', 'pd.meditem')
            .where('p.vn', seq)
            .andWhere('med.type', 'in', [1, 3, 5])
            .groupBy('pd.meditem','usage_line1','usage_line2')
            .orderByRaw('date_serv DESC');
        return data;
    }

    async getLabs_old(db: Knex, hn: any, dateServe: any, seq: any, referno: any) {
        let data = await db.raw(`
        SELECT date_serv,time_serv,labgroup,lab_name,lab_result,unit,standard_result 
		FROM 
        (select 
            DATE_FORMAT(date(l.vstdttm),'%Y-%m-%d') as date_serv, 
            DATE_FORMAT(time(l.vstdttm),'%h:%i:%s') as time_serv,
            r.lab_name as labgroup,
            replace(l.fieldlabel,"'",'\`') as lab_name,
            replace(replace(r.labresult,char(13),' '),char(10),' ') as lab_result,
            r.unit,
            r.normal as standard_result
            from 
            labresult as r
            inner join 
            (select 
                max(l.ln) as ln,
                l.labcode,
                l.vstdttm,
                lb.fieldlabel,
                lb.fieldname
            from lbbk as l 
            inner join lablabel as lb on l.labcode = lb.labcode 
            where l.vn ='${seq}' and l.finish=1
            group by labcode) as l on r.ln=l.ln  and r.lab_code_local=l.fieldname
            where 
            (r.labcode != NULL or r.labcode !='')
        ) x
		GROUP BY date_serv,time_serv,lab_name
		ORDER BY date_serv,time_serv,labgroup 
        `
        );
        return data[0];
    }

    async getLabs(db:Knex, hn:any, dateServe:any, seq:any, referno:any) {
        const subquery = db('lbbk as l')
          .select(
            db.raw('MAX(l.ln) as ln'),
            'l.labcode',
            'l.vstdttm',
            'lb.fieldlabel',
            'lb.fieldname'
          )
          .innerJoin('lablabel as lb', 'l.labcode', 'lb.labcode')
          .where('l.vn', seq)
          .andWhere('l.finish', 1)
          .groupBy('l.labcode');
      
        const query = db
          .select(
            db.raw("DATE_FORMAT(DATE(l.vstdttm), '%Y-%m-%d') as date_serv"),
            db.raw("DATE_FORMAT(TIME(l.vstdttm), '%h:%i:%s') as time_serv"),
            'r.lab_name as labgroup',
            db.raw("REPLACE(l.fieldlabel, \"'\", '`') as lab_name"),
            db.raw(
              "REPLACE(REPLACE(r.labresult, CHAR(13), ' '), CHAR(10), ' ') as lab_result"
            ),
            'r.unit',
            'r.normal as standard_result'
          )
          .from('labresult as r')
          .innerJoin(
            subquery.as('l'),
            function () {
              this.on('r.ln', '=', 'l.ln').andOn(
                'r.lab_code_local',
                '=',
                'l.fieldname'
              );
            }
          )
          .where(function () {
            this.whereNotNull('r.labcode').orWhere('r.labcode', '!=', '');
          });
      
        const data = await db
          .from(query.as('x'))
          .select(
            'date_serv',
            'time_serv',
            'labgroup',
            'lab_name',
            'lab_result',
            'unit',
            'standard_result'
          )
          .groupBy('date_serv', 'time_serv', 'lab_name')
          .orderBy(['date_serv', 'time_serv', 'labgroup']);
      
        return data;
      }

    async getAppointment_old(db: Knex, hn: any, dateServ: any, seq: any, referno: any) {
        let data = await db.raw(`
        SELECT o.vn as seq, o.vstdate as date_serv, o.fudate as date, o.futime as time, o.cln as department, o.dscrptn as detail, time(ovst.vstdttm) as time_serv
        FROM oapp as o 
        INNER JOIN ovst on ovst.vn = o.vn
        WHERE o.vn ='${seq}'
        `);
        return data[0];
    }

    async getAppointment(db: Knex, hn: any, dateServ: any, seq: any, referno: any) {
        const data = await db('oapp as o')
          .select(
            'o.vn as seq',
            'o.vstdate as date_serv',
            'o.fudate as date',
            'o.futime as time',
            'o.cln as department',
            'o.dscrptn as detail',
            db.raw('TIME(ovst.vstdttm) as time_serv')
          )
          .innerJoin('ovst', 'ovst.vn', 'o.vn')
          .where('o.vn', seq);
      
        return data;
      }

    async getVaccine_old(db: Knex, hn: any, referno: any) {
        let data = await db.raw(`
        select 
        o.vstdttm as date_serv,
        DATE_FORMAT(time(o.drxtime),'%h:%i:%s') as time_serv, 
        h.vac as vaccine_code, 
        h.namehpt as vaccine_name
        from 
        epi e 
        inner join 
        ovst o on e.vn = o.vn 
        left join 
        hpt as h on e.vac=h.codehpt
        where 
        o.hn='${hn}'
        
        UNION

        select 
        o.vstdttm as date_serv,
        DATE_FORMAT(time(o.drxtime),'%h:%i:%s') as time_serv, 
        vc.stdcode as vacine_code, 
        vc.\`name\` as vacine_name
        from 
        ovst o 
        inner join 
        prsc pc on o.vn = pc.vn  
        inner join 
        prscdt pd on pc.prscno = pd.prscno  
        inner join 
        meditem m on pd.meditem = m.meditem 
        inner join 
        vaccine vc on vc.meditem = m.meditem  
        where 
        o.hn='${hn}'
        `);
        return data[0];
    }

    async getVaccine(db: Knex, hn: any, referno: any) {
        // Query 1: ข้อมูลจากตาราง `epi`, `ovst`, และ `hpt`
        const query1 = db('epi as e')
          .select(
            'o.vstdttm as date_serv',
            db.raw("DATE_FORMAT(TIME(o.drxtime), '%h:%i:%s') as time_serv"),
            'h.vac as vaccine_code',
            'h.namehpt as vaccine_name'
          )
          .innerJoin('ovst as o', 'e.vn', 'o.vn')
          .leftJoin('hpt as h', 'e.vac', 'h.codehpt')
          .where('o.hn', hn);
      
        // Query 2: ข้อมูลจากตาราง `ovst`, `prsc`, `prscdt`, `meditem`, และ `vaccine`
        const query2 = db('ovst as o')
          .select(
            'o.vstdttm as date_serv',
            db.raw("DATE_FORMAT(TIME(o.drxtime), '%h:%i:%s') as time_serv"),
            'vc.stdcode as vaccine_code',
            'vc.name as vaccine_name'
          )
          .innerJoin('prsc as pc', 'o.vn', 'pc.vn')
          .innerJoin('prscdt as pd', 'pc.prscno', 'pd.prscno')
          .innerJoin('meditem as m', 'pd.meditem', 'm.meditem')
          .innerJoin('vaccine as vc', 'vc.meditem', 'm.meditem')
          .where('o.hn', hn);
      
        // Combine queries with UNION
        const data = await db.union([query1, query2]);
      
        return data;
      }
    async getProcedure_old(db: Knex, hn: any, dateServe: any, seq: any, referno: any) {
        let data = await db.raw(`
        SELECT
        o.hn as pid,
        o.vn as seq,
        DATE_FORMAT(date(o.vstdttm),'%Y%m%d') as date_serv,	
        DATE_FORMAT(time(o.nrxtime),'%h:%i:%s') as time_serv, 
        p.icd9cm as procedure_code,	
        p.icd9name as procedure_name,
        DATE_FORMAT(date(p.opdttm),'%Y%m%d') as start_date,	
        DATE_FORMAT(time(p.opdttm),'%h:%i:%s') as start_time,
        DATE_FORMAT(date(p.opdttm),'%Y%m%d') as end_date,
        '00:00:00' as end_time
        from
        ovst o 
        inner join 
        ovstdx ox on o.vn = ox.vn 
        inner join
        oprt p on o.vn = p.vn 
        left outer join
        cln c on o.cln = c.cln
        LEFT OUTER JOIN 
        dct on (
            CASE WHEN LENGTH(o.dct) = 5 THEN dct.lcno = o.dct 
                WHEN LENGTH(o.dct) = 4 THEN dct.dct = substr(o.dct,1,2)  
                WHEN LENGTH(o.dct) = 2 THEN dct.dct = o.dct END )
        where 
        o.vn = '${seq}' and p.an = 0 
        group by 
        p.vn,procedure_code
        UNION 
        SELECT 
        o.hn as pid,
        o.vn as seq,
        DATE_FORMAT(date(o.vstdttm),'%Y%m%d') as date_serv,
        DATE_FORMAT(time(o.nrxtime),'%h:%i:%s') as time_serv, 
        i.ICD10TM as procedure_code,
        i.name_Tx as procedure_name,
        DATE_FORMAT(date(dt.vstdttm),'%Y%m%d') as start_date,	
        DATE_FORMAT(time(dt.vstdttm),'%h:%i:%s') as start_time,
        DATE_FORMAT(date(dt.vstdttm),'%Y%m%d') as end_date,
        '00:00:00' as end_time
    
        FROM
        dtdx 
        INNER JOIN 
        icd9dent as i on dtdx.dttx=i.code_tx
        INNER JOIN 
        dt on dtdx.dn=dt.dn
        INNER JOIN
        ovst as o on dt.vn=o.vn and o.cln='40100'
        left outer join 
        cln c on o.cln = c.cln  
        left join dentist as d on dt.dnt=d.codedtt
        where 
        o.vn = '${seq}'
        group by 
        dtdx.dn,procedure_code
        `);
        return data[0];
    }

    async getProcedure(db: Knex, hn: any, dateServe: any, seq: any, referno: any) {
        // Query 1: ข้อมูลจาก `ovst`, `ovstdx`, และ `oprt`
        const query1 = db('ovst as o')
          .select(
            'o.hn as pid',
            'o.vn as seq',
            db.raw("DATE_FORMAT(DATE(o.vstdttm), '%Y%m%d') as date_serv"),
            db.raw("DATE_FORMAT(TIME(o.nrxtime), '%h:%i:%s') as time_serv"),
            'p.icd9cm as procedure_code',
            'p.icd9name as procedure_name',
            db.raw("DATE_FORMAT(DATE(p.opdttm), '%Y%m%d') as start_date"),
            db.raw("DATE_FORMAT(TIME(p.opdttm), '%h:%i:%s') as start_time"),
            db.raw("DATE_FORMAT(DATE(p.opdttm), '%Y%m%d') as end_date"),
            db.raw("'00:00:00' as end_time")
          )
          .innerJoin('ovstdx as ox', 'o.vn', 'ox.vn')
          .innerJoin('oprt as p', 'o.vn', 'p.vn')
          .leftJoin('cln as c', 'o.cln', 'c.cln')
          .leftJoin('dct', function () {
            this.on(db.raw(`
              CASE 
                WHEN LENGTH(o.dct) = 5 THEN dct.lcno = o.dct 
                WHEN LENGTH(o.dct) = 4 THEN dct.dct = SUBSTR(o.dct, 1, 2) 
                WHEN LENGTH(o.dct) = 2 THEN dct.dct = o.dct 
              END
            `));
          })
          .where('o.vn', seq)
          .andWhere('p.an', 0)
          .groupBy('p.vn', 'procedure_code');
      
        // Query 2: ข้อมูลจาก `dtdx`, `icd9dent`, `dt`, และ `ovst`
        const query2 = db('dtdx')
          .select(
            'o.hn as pid',
            'o.vn as seq',
            db.raw("DATE_FORMAT(DATE(o.vstdttm), '%Y%m%d') as date_serv"),
            db.raw("DATE_FORMAT(TIME(o.nrxtime), '%h:%i:%s') as time_serv"),
            'i.ICD10TM as procedure_code',
            'i.name_Tx as procedure_name',
            db.raw("DATE_FORMAT(DATE(dt.vstdttm), '%Y%m%d') as start_date"),
            db.raw("DATE_FORMAT(TIME(dt.vstdttm), '%h:%i:%s') as start_time"),
            db.raw("DATE_FORMAT(DATE(dt.vstdttm), '%Y%m%d') as end_date"),
            db.raw("'00:00:00' as end_time")
          )
          .innerJoin('icd9dent as i', 'dtdx.dttx', 'i.code_tx')
          .innerJoin('dt', 'dtdx.dn', 'dt.dn')
          .innerJoin('ovst as o', function () {
            this.on(
              db.raw('dt.vn = o.vn AND o.cln = ?', ['40100'])
            );
          })
          .leftJoin('cln as c', 'o.cln', 'c.cln')
          .leftJoin('dentist as d', 'dt.dnt', 'd.codedtt')
          .where('o.vn', seq)
          .groupBy('dtdx.dn', 'procedure_code');
      
        // Combine queries with UNION
        const data = await db.union([query1, query2]);
      
        return data;
      }

    async getNurture_old(db: Knex, hn: any, dateServe: any, seq: any, referno: any) {
        let data = await db.raw(`
        SELECT x.seq,x.date_serv,x.time_serv,x.bloodgrp,x.weight,x.height,x.bmi,x.temperature,x.pr,x.rr,x.sbp,x.dbp,
        x.symptom,x.depcode,x.department,x.movement_score,x.vocal_score,x.eye_score,x.oxygen_sat,x.gak_coma_sco,x.diag_text,x.pupil_right,x.pupil_left
        FROM (
		select o.vn as seq,
        DATE_FORMAT(date(o.rgtdate),'%Y-%m-%d') as date_serv, 
        DATE_FORMAT(time(o.rgttime),'%h:%i:%s') as time_serv, 
        p.bloodgrp as bloodgrp,
        o.bw as weight,
        o.height as height,
        o.bmi as bmi,
        ifnull(t.tt,'') as temperature,
        ifnull(t.pr,'') as pr,
        ifnull(t.rr,'') as rr,
        ifnull(t.sbp,'') as sbp,
        ifnull(t.dbp,'') as dbp,
		concat(ifnull(group_concat(cc.symptom  order by cc.id separator ' '),''),ifnull(group_concat(cc_d.symptom  order by cc_d.id separator ' '),'')) as symptom,
        c.idpm as depcode,
        c.nameidpm as department,
        '' as movement_score,'' as vocal_score,'' as eye_score,'' as oxygen_sat,'' as gak_coma_sco,
		vd.diagtext as diag_text,'' as pupil_right, '' as pupil_left
        FROM ipt as o 
		LEFT JOIN visitdiagtext as vd on o.vn = vd.vn 
		LEFT JOIN symptm as cc on o.vn = cc.vn
        LEFT JOIN dt on o.vn=dt.vn and o.an=0
        LEFT JOIN symp_d as cc_d on dt.dn=cc_d.dn 
        left join (select * from tpr order by dttm desc) as t on o.an=t.an
        LEFT JOIN idpm as c ON c.idpm = o.ward 
        INNER JOIN pt as p	ON p.hn = o.hn
        WHERE o.vn = '${seq}'
		UNION
		select o.vn as seq,
        DATE_FORMAT(date(o.vstdttm),'%Y-%m-%d') as date_serv, 
        DATE_FORMAT(time(o.vstdttm),'%h:%i:%s') as time_serv, 
        p.bloodgrp as bloodgrp,
        o.bw as weight,
        o.height as height,
        o.bmi as bmi,
        o.tt as temperature,
        o.pr as pr,
        o.rr as rr,
        o.sbp as sbp,
        o.dbp as dbp,
        concat(ifnull(group_concat(cc.symptom  order by cc.id separator ' '),''),ifnull(group_concat(cc_d.symptom  order by cc_d.id separator ' '),'')) as symptom,
        c.cln as depcode,
        c.namecln as department,
        s.m as movement_score,s.v as vocal_score,s.e as eye_score,s.o2sat as oxygen_sat,s.sos as gak_coma_sco,
		vd.diagtext  as diag_text,'' as pupil_right, '' as pupil_left
        FROM ovst as o 
        LEFT JOIN visitdiagtext as vd on o.vn = vd.vn        
		LEFT JOIN symptm as cc on o.vn = cc.vn
        LEFT JOIN dt on o.vn=dt.vn and o.an=0
        LEFT JOIN symp_d as cc_d on dt.dn=cc_d.dn 
        LEFT JOIN visitsosscore as s on o.vn=s.vn
        LEFT JOIN cln as c ON c.cln = o.cln 
        INNER JOIN pt as p	ON p.hn = o.hn
        WHERE o.vn = '${seq}' and o.an = 0 
		) x WHERE x.seq is not NULL  and x.seq !='0'
        `);
        return data[0];
    }

    async getNurture(db: Knex, hn: any, dateServe: any, seq: any, referno: any) {
        const data = await db
          .select([
            'x.seq',
            'x.date_serv',
            'x.time_serv',
            'x.bloodgrp',
            'x.weight',
            'x.height',
            'x.bmi',
            'x.temperature',
            'x.pr',
            'x.rr',
            'x.sbp',
            'x.dbp',
            'x.symptom',
            'x.depcode',
            'x.department',
            'x.movement_score',
            'x.vocal_score',
            'x.eye_score',
            'x.oxygen_sat',
            'x.gak_coma_sco',
            'x.diag_text',
            'x.pupil_right',
            'x.pupil_left',
          ])
          .from(
            db
              .select([
                'o.vn as seq',
                db.raw("DATE_FORMAT(date(o.rgtdate), '%Y-%m-%d') as date_serv"),
                db.raw("DATE_FORMAT(time(o.rgttime), '%h:%i:%s') as time_serv"),
                'p.bloodgrp as bloodgrp',
                'o.bw as weight',
                'o.height as height',
                'o.bmi as bmi',
                db.raw("IFNULL(t.tt, '') as temperature"),
                db.raw("IFNULL(t.pr, '') as pr"),
                db.raw("IFNULL(t.rr, '') as rr"),
                db.raw("IFNULL(t.sbp, '') as sbp"),
                db.raw("IFNULL(t.dbp, '') as dbp"),
                db.raw(
                  "CONCAT(IFNULL(GROUP_CONCAT(cc.symptom ORDER BY cc.id SEPARATOR ' '), ''), IFNULL(GROUP_CONCAT(cc_d.symptom ORDER BY cc_d.id SEPARATOR ' '), '')) as symptom"
                ),
                'c.idpm as depcode',
                'c.nameidpm as department',
                db.raw("'' as movement_score"),
                db.raw("'' as vocal_score"),
                db.raw("'' as eye_score"),
                db.raw("'' as oxygen_sat"),
                db.raw("'' as gak_coma_sco"),
                'vd.diagtext as diag_text',
                db.raw("'' as pupil_right"),
                db.raw("'' as pupil_left"),
              ])
              .from('ipt as o')
              .leftJoin('visitdiagtext as vd', 'o.vn', 'vd.vn')
              .leftJoin('symptm as cc', 'o.vn', 'cc.vn')
              .leftJoin('dt', function () {
                this.on('o.vn', '=', 'dt.vn').andOn('o.an', '=', db.raw('0'));
              })
              .leftJoin('symp_d as cc_d', 'dt.dn', 'cc_d.dn')
              .leftJoin(
                db.raw('(SELECT * FROM tpr ORDER BY dttm DESC) as t'),
                'o.an',
                't.an'
              )
              .leftJoin('idpm as c', 'c.idpm', 'o.ward')
              .innerJoin('pt as p', 'p.hn', 'o.hn')
              .where('o.vn', seq)
              .union([
                db
                  .select([
                    'o.vn as seq',
                    db.raw(
                      "DATE_FORMAT(date(o.vstdttm), '%Y-%m-%d') as date_serv"
                    ),
                    db.raw(
                      "DATE_FORMAT(time(o.vstdttm), '%h:%i:%s') as time_serv"
                    ),
                    'p.bloodgrp as bloodgrp',
                    'o.bw as weight',
                    'o.height as height',
                    'o.bmi as bmi',
                    'o.tt as temperature',
                    'o.pr as pr',
                    'o.rr as rr',
                    'o.sbp as sbp',
                    'o.dbp as dbp',
                    db.raw(
                      "CONCAT(IFNULL(GROUP_CONCAT(cc.symptom ORDER BY cc.id SEPARATOR ' '), ''), IFNULL(GROUP_CONCAT(cc_d.symptom ORDER BY cc_d.id SEPARATOR ' '), '')) as symptom"
                    ),
                    'c.cln as depcode',
                    'c.namecln as department',
                    's.m as movement_score',
                    's.v as vocal_score',
                    's.e as eye_score',
                    's.o2sat as oxygen_sat',
                    's.sos as gak_coma_sco',
                    'vd.diagtext as diag_text',
                    db.raw("'' as pupil_right"),
                    db.raw("'' as pupil_left"),
                  ])
                  .from('ovst as o')
                  .leftJoin('visitdiagtext as vd', 'o.vn', 'vd.vn')
                  .leftJoin('symptm as cc', 'o.vn', 'cc.vn')
                  .leftJoin('dt', function () {
                    this.on('o.vn', '=', 'dt.vn').andOn('o.an', '=', db.raw('0'));
                  })
                  .leftJoin('symp_d as cc_d', 'dt.dn', 'cc_d.dn')
                  .leftJoin('visitsosscore as s', 'o.vn', 's.vn')
                  .leftJoin('cln as c', 'c.cln', 'o.cln')
                  .innerJoin('pt as p', 'p.hn', 'o.hn')
                  .where('o.vn', seq)
                  .andWhere('o.an', '=', 0),
              ])
              .as('x')
          )
          .whereNotNull('x.seq')
          .andWhere('x.seq', '!=', '0');
      
        return data;
      }

    async getPhysical_old(db: Knex, hn: any, dateServe: any, seq: any, referno: any) {
        let data = await db.raw(`
        SELECT r.seq,group_concat(r.pe) as pe FROM (
        SELECT d.vn as seq , group_concat(s.sign order by id separator '\n') as pe FROM dt as d inner join sign_d as s on d.dn=s.dn WHERE d.vn = '${seq}'
        UNION 
        SELECT s.vn as seq , group_concat(s.sign order by id separator '\n') as pe FROM sign as s INNER join ovst as o on o.vn=s.vn WHERE s.vn = '${seq}' 
        ) as r WHERE seq is not null  and seq !='0' group by r.seq
        `);
        return data[0];
    }

    async getPhysical(db: Knex, hn: any, dateServe: any, seq: any, referno: any) {
        const data = await db
          .select(['r.seq', db.raw('GROUP_CONCAT(r.pe) as pe')])
          .from(function (x: any) { 
            x.select([
              'd.vn as seq',
              db.raw("GROUP_CONCAT(s.sign ORDER BY s.id SEPARATOR '\\n') as pe"),
            ])
              .from('dt as d')
              .innerJoin('sign_d as s', 'd.dn', 's.dn')
              .where('d.vn', seq)
              .union([
                db
                  .select([
                    's.vn as seq',
                    db.raw(
                      "GROUP_CONCAT(s.sign ORDER BY s.id SEPARATOR '\\n') as pe"
                    ),
                  ])
                  .from('sign as s')
                  .innerJoin('ovst as o', 'o.vn', 's.vn')
                  .where('s.vn', seq),
              ])
              .as('r');
          })
          .whereNotNull('r.seq')
          .andWhere('r.seq', '!=', '0')
          .groupBy('r.seq');
      
        return data;
      }

    async getPillness_old(db: Knex, hn: any, dateServe: any, seq: any, referno: any) {
        let data = await db.raw(`
        SELECT p.vn as seq , group_concat(p.pillness order by id separator '\n') as hpi FROM pillness as p inner join ovst as o on p.vn=o.vn and o.an=0 WHERE p.vn = '${seq}'  group by p.vn
        `);
        return data[0];
    }

    async getPillness(db: Knex, hn: any, dateServe: any, seq: any, referno: any) {
        const data = await db
          .select([
            'p.vn as seq',
            db.raw("GROUP_CONCAT(p.pillness ORDER BY p.id SEPARATOR '\\n') as hpi")
          ])
          .from('pillness as p')
          .innerJoin('ovst as o', function() {
            this.on('p.vn', '=', 'o.vn').andOn('o.an', '=', db.raw('0'));
          })
          .where('p.vn', seq)
          .groupBy('p.vn');
      
        return data;
      }
    async getBedsharing_old(db: Knex) {
        let data = await db.raw(`
        select 
        idpm.idpm as ward_code,
        idpm.nameidpm as ward_name,
        count(an) as ward_pt,
        idpm.bed as ward_bed,
        idpm.bed_std as ward_standard 
        from 
        idpm 
        left join ipt on idpm.idpm=ipt.ward
        and dchdate = '0000-00-00'
        group by idpm
        `);
        return data[0];
    }

    async getBedsharing(db: Knex) {
        const data = await db('idpm')
          .leftJoin('ipt', function() {
            this.on('idpm.idpm', '=', 'ipt.ward').andOn('ipt.dchdate', '=', db.raw("'0000-00-00'"));
          })
          .select([
            'idpm.idpm as ward_code',
            'idpm.nameidpm as ward_name',
            db.raw('COUNT(ipt.an) as ward_pt'),
            'idpm.bed as ward_bed',
            'idpm.bed_std as ward_standard'
          ])
          .groupBy('idpm.idpm', 'idpm.nameidpm', 'idpm.bed', 'idpm.bed_std');
      
        return data;
      }

    async getReferOut_old(db: Knex, start_date: any, end_date: any) {
        let data = await db.raw(`
        select o.vn as seq, pt.hn as hn, o.an as an,
        pt.pname as title_name, pt.fname as first_name, pt.lname as last_name,
        cast(replace(o.rfrno,"/",'-') as char(20) CHARACTER SET UTF8) as referno,
        date(o.vstdate) as referdate,
        if(o.cln='' or o.cln is null,i.nameidpm,c.namecln) as location_name,
        o.rfrlct as to_hcode,
        if(t.trauma='T',1,2) as pttype_id,
        if(t.trauma='T','Trauma','Non Trauma') as pttype_name,
        t.triage as strength_id,
        (case t.triage 
            when 1 then 'Resucitate'
            when 2 then 'Emergency'
            when 3 then 'Urgency'
            when 4 then 'Semi Urgency'
            when 5 then 'Non Urgency'
            else '' end 
            ) as strength_name,
        h.namehosp as to_hcode_name,
        concat(ifnull(f.namerfrcs,''),' Consult:',rt.nametype) as refer_cause,
        time(o.vsttime*100) as refertime,
        concat(d.lcno,',',d.cid) as doctor,
        CONCAT(d.fname, ' ', d.lname) as doctor_name
        ,replace(replace(vc.detail,char(13),' '),char(10),' ') as refer_remark
        from orfro as o
        left Join hospcode as h on h.off_id = o.rfrlct
        inner join ovst on ovst.vn = o.vn
        inner Join pt on pt.hn = ovst.hn
        left join cln as c on o.cln=c.cln 
        left join idpm as i on o.ward=i.idpm
        left Join rfrcs as f on f.rfrcs = o.rfrcs
        left Join dct as d on (case length(ovst.dct) when 5 then d.lcno=ovst.dct when 4 then substr(ovst.dct,1,2) = d.dct end)
        left join optriage as t on o.vn=t.vn
        left join l_rfrtype as rt on o.rfrtype=rt.rfrtype
        left join visitnoteconsult as vc on o.vn=vc.vn
        where date(o.vstdate) between '${start_date}' and '${end_date}'  GROUP BY seq,referno
        `);
        return data[0];
    }

    async getReferOut(db: Knex, start_date: any, end_date: any) {
        const data = await db('orfro as o')
          .leftJoin('hospcode as h', 'h.off_id', '=', 'o.rfrlct')
          .innerJoin('ovst', 'ovst.vn', '=', 'o.vn')
          .innerJoin('pt', 'pt.hn', '=', 'ovst.hn')
          .leftJoin('cln as c', 'o.cln', '=', 'c.cln')
          .leftJoin('idpm as i', 'o.ward', '=', 'i.idpm')
          .leftJoin('rfrcs as f', 'f.rfrcs', '=', 'o.rfrcs')
          .leftJoin('dct as d', function() {
            this.on(db.raw('LENGTH(ovst.dct) = 5'))
              .andOn('d.lcno', '=', 'ovst.dct')
              .orOn(db.raw('LENGTH(ovst.dct) = 4'))
              .andOn(db.raw('SUBSTRING(ovst.dct, 1, 2) = d.dct'));
          })
          .leftJoin('optriage as t', 'o.vn', '=', 't.vn')
          .leftJoin('l_rfrtype as rt', 'o.rfrtype', '=', 'rt.rfrtype')
          .leftJoin('visitnoteconsult as vc', 'o.vn', '=', 'vc.vn')
          .select([
            'o.vn as seq',
            'pt.hn as hn',
            'o.an as an',
            'pt.pname as title_name',
            'pt.fname as first_name',
            'pt.lname as last_name',
            db.raw('CAST(REPLACE(o.rfrno, "/", "-") AS CHAR(20) CHARACTER SET UTF8) as referno'),
            db.raw('DATE(o.vstdate) as referdate'),
            db.raw('IF(o.cln = "" OR o.cln IS NULL, i.nameidpm, c.namecln) as location_name'),
            'o.rfrlct as to_hcode',
            db.raw('IF(t.trauma = "T", 1, 2) as pttype_id'),
            db.raw('IF(t.trauma = "T", "Trauma", "Non Trauma") as pttype_name'),
            't.triage as strength_id',
            db.raw(`
              CASE t.triage
                WHEN 1 THEN 'Resucitate'
                WHEN 2 THEN 'Emergency'
                WHEN 3 THEN 'Urgency'
                WHEN 4 THEN 'Semi Urgency'
                WHEN 5 THEN 'Non Urgency'
                ELSE '' 
              END as strength_name
            `),
            'h.namehosp as to_hcode_name',
            db.raw('CONCAT(IFNULL(f.namerfrcs, ""), " Consult:", rt.nametype) as refer_cause'),
            db.raw('TIME(o.vsttime * 100) as refertime'),
            db.raw('CONCAT(d.lcno, ",", d.cid) as doctor'),
            db.raw('CONCAT(d.fname, " ", d.lname) as doctor_name'),
            db.raw('REPLACE(REPLACE(vc.detail, CHAR(13), " "), CHAR(10), " ") as refer_remark')
          ])
          .whereRaw('DATE(o.vstdate) BETWEEN ? AND ?', [start_date, end_date])
          .groupBy('seq', 'referno');
      
        return data;
      }

    async getReferBack_old(db: Knex, start_date: any, end_date: any) {
        let data = await db.raw(`
        select o.vn as seq, pt.hn as hn, o.an as an,
        pt.pname as title_name,
        pt.fname as first_name, 
        pt.lname as last_name,
        cast(replace(o.rfrno,"/",'-') as char(20) CHARACTER SET UTF8) as referno,
        date(ovst.vstdttm) as referdate,
        o.hcode as to_hcode,
        h.namehosp as to_hcode_name,
        '' as refer_cause,
        time(ovst.vstdttm*100) as refertime,
        concat(d.lcno,',',d.cid) as doctor,
        CONCAT(d.fname, ' ', d.lname) as doctor_name
        from referresult as o
        inner Join hospcode as h on h.off_id = o.hcode
        INNER join ovst on ovst.vn = o.vn
        INNER join ovstdx on ovst.vn=ovstdx.vn and ovstdx.cnt = 1 
        INNER Join pt on pt.hn = ovst.hn
        LEFT Join dct as d on d.lcno = ovst.dct
        where date(ovst.vstdttm) between '${start_date}' and '${end_date}' 
        union
        select o.vn as seq, pt.hn as hn, o.an as an,
        pt.pname as title_name,
        pt.fname as first_name, 
        pt.lname as last_name,
        cast(replace(o.rfrno,"/",'-') as char(20) CHARACTER SET UTF8) as referno,
        date(o.dchdate) as referdate,
        o.hcode as to_hcode,
        h.namehosp as to_hcode_name,
        '' as refer_cause,
        time(o.dchtime*100) as refertime,
        d.lcno as doctor,
        CONCAT(d.fname, ' ', d.lname) as doctor_name
        from referresult as o
        inner Join hospcode as h on h.off_id = o.hcode
        inner join ipt on ipt.an = o.an
        inner join iptdx on ipt.an=iptdx.an and itemno=1 
        inner Join pt on pt.hn = ipt.hn
        LEFT Join dct as d on d.lcno = iptdx.dct
        where o.dchdate between '${start_date}' and '${end_date}'
        `);
        return data[0];
    }

    async getReferBack(db: Knex, start_date: any, end_date: any) {
        const data = await db
          .select([
            'o.vn as seq',
            'pt.hn as hn',
            'o.an as an',
            'pt.pname as title_name',
            'pt.fname as first_name',
            'pt.lname as last_name',
            db.raw('CAST(REPLACE(o.rfrno, "/", "-") AS CHAR(20) CHARACTER SET UTF8) as referno'),
            db.raw('DATE(ovst.vstdttm) as referdate'),
            'o.hcode as to_hcode',
            'h.namehosp as to_hcode_name',
            db.raw('"" as refer_cause'),
            db.raw('TIME(ovst.vstdttm * 100) as refertime'),
            db.raw('CONCAT(d.lcno, ",", d.cid) as doctor'),
            db.raw('CONCAT(d.fname, " ", d.lname) as doctor_name')
          ])
          .from('referresult as o')
          .innerJoin('hospcode as h', 'h.off_id', '=', 'o.hcode')
          .innerJoin('ovst', 'ovst.vn', '=', 'o.vn')
          .innerJoin('ovstdx', function() {
            this.on('ovst.vn', '=', 'ovstdx.vn').andOn('ovstdx.cnt', '=', db.raw('1'));
          })
          .innerJoin('pt', 'pt.hn', '=', 'ovst.hn')
          .leftJoin('dct as d', 'd.lcno', '=', 'ovst.dct')
          .whereRaw('DATE(ovst.vstdttm) BETWEEN ? AND ?', [start_date, end_date])
          .union(function() {
            this.select([
              'o.vn as seq',
              'pt.hn as hn',
              'o.an as an',
              'pt.pname as title_name',
              'pt.fname as first_name',
              'pt.lname as last_name',
              db.raw('CAST(REPLACE(o.rfrno, "/", "-") AS CHAR(20) CHARACTER SET UTF8) as referno'),
              db.raw('DATE(o.dchdate) as referdate'),
              'o.hcode as to_hcode',
              'h.namehosp as to_hcode_name',
              db.raw('"" as refer_cause'),
              db.raw('TIME(o.dchtime * 100) as refertime'),
              'd.lcno as doctor',
              db.raw('CONCAT(d.fname, " ", d.lname) as doctor_name')
            ])
              .from('referresult as o')
              .innerJoin('hospcode as h', 'h.off_id', '=', 'o.hcode')
              .innerJoin('ipt', 'ipt.an', '=', 'o.an')
              .innerJoin('iptdx', function() {
                this.on('ipt.an', '=', 'iptdx.an').andOn('iptdx.itemno', '=', db.raw('1'));
              })
              .innerJoin('pt', 'pt.hn', '=', 'ipt.hn')
              .leftJoin('dct as d', 'd.lcno', '=', 'iptdx.dct')
              .whereRaw('o.dchdate BETWEEN ? AND ?', [start_date, end_date]);
          });
      
        return data;
      }

    async getAppoint_old(db: Knex, hn: any, app_date: any, referno: any) {
        let data = await db.raw(`
        SELECT 
        a.vn as seq,
        a.fudate as receive_apppoint_date,
        '' as receive_apppoint_beginttime,
        '' as receive_apppoint_endtime,
        CONCAT(d.dspname,' ',l.prename,d.fname,' ',d.lname) as receive_apppoint_doctor,
        group_concat(distinct c.namecln) as receive_apppoint_chinic, 
        group_concat(distinct a.dscrptn) as receive_text,
        '' as receive_by
        from oapp as a 
        inner join pt as p on a.hn=p.hn
        left join dct as d on (case length(a.dct) when 5 then a.dct=d.lcno when 4 then substr(a.dct,3,2)=d.dct end)
        left join l_prename as l on d.pname=l.prename_code
        left join lab on substr(a.cln,2,3)=lab.labcode
        left join cln as c on if(substr(a.cln,1,1) !=7,a.cln=c.cln,c.cln='70100')
        WHERE a.hn ='${hn}' and a.vstdate = '${app_date}'
        `);
        return data[0];
    }

    async getAppoint(db: Knex, hn: any, app_date: any, referno: any) {
        const data = await db
          .select([
            'a.vn as seq',
            'a.fudate as receive_apppoint_date',
            db.raw('"" as receive_apppoint_beginttime'),
            db.raw('"" as receive_apppoint_endtime'),
            db.raw('CONCAT(d.dspname, " ", l.prename, d.fname, " ", d.lname) as receive_apppoint_doctor'),
            db.raw('GROUP_CONCAT(DISTINCT c.namecln) as receive_apppoint_chinic'),
            db.raw('GROUP_CONCAT(DISTINCT a.dscrptn) as receive_text'),
            db.raw('"" as receive_by')
          ])
          .from('oapp as a')
          .innerJoin('pt as p', 'a.hn', '=', 'p.hn')
          .leftJoin('dct as d', function() {
            this.on(db.raw('CASE LENGTH(a.dct) WHEN 5 THEN a.dct = d.lcno WHEN 4 THEN SUBSTR(a.dct, 3, 2) = d.dct END'));
          })
          .leftJoin('l_prename as l', 'd.pname', '=', 'l.prename_code')
          .leftJoin('lab', function() {
            this.on(db.raw('SUBSTR(a.cln, 2, 3)  = lab.labcode'));
          })
          .leftJoin('cln as c', function() {
            this.on(db.raw('IF(SUBSTR(a.cln, 1, 1) != 7, a.cln = c.cln, c.cln = "70100")'));
          })
          .where('a.hn', hn)
          .andWhere('a.vstdate', app_date);
      
        return data;
      }

    async getXray_old(db: Knex, hn: any, dateServe: any, seq: any, referno: any) {
        let data = await db.raw(`
        SELECT xryrgt.vstdate as xray_date ,
        xray.xryname as xray_name 
        FROM xryrgt 
        INNER JOIN xray on xray.xrycode = xryrgt.xrycode
        WHERE xryrgt.vn = '${seq}'
        `);
        return data[0];
    }

    async getXray(db: Knex, hn: any, dateServe: any, seq: any, referno: any) {
        const data = await db
          .select('xryrgt.vstdate as xray_date', 'xray.xryname as xray_name')
          .from('xryrgt')
          .innerJoin('xray', 'xray.xrycode', '=', 'xryrgt.xrycode')
          .where('xryrgt.vn', seq);
      
        return data;
      }


    async getDepartment_old(db: Knex) {
        let data = await db.raw(`
        select cln as dep_code,namecln as dep_name from cln
        UNION ALL
        SELECT 
        "IPD" as dep_code, "IPD" as dep_name
        `);
        return data[0];
    }

    async getDepartment(db: Knex) {
        const data = await db
          .select('cln as dep_code', 'namecln as dep_name')
          .from('cln')
          .unionAll(function() {
            this.select(db.raw('"IPD" as dep_code'), db.raw('"IPD" as dep_name'));
          });
      
        return data;
      }

    async getPtHN_old(db: Knex, cid: any) {
        let data = await db.raw(`
        select hn from pt where pt.pop_id = '${cid}'
        `);
        return data[0][0];
    }

    async getPtHN(db: Knex, cid: any) {
        const data = await db('pt')
          .select('hn')
          .where('pop_id', cid)
          .limit(1); // ใช้ .first() เพื่อดึงแค่แถวแรก
      
        return data[0];
      }
    async getMedrecconcile_old(db: Knex, hn: any) {
        let data = await db.raw(`
        select '' as drug_hospcode ,hospname as drug_hospname,medname as drug_name,meduse as drug_use,lastdate as drug_receive_date from medreconcile where hn = '${hn}'
        `);
        return data[0];
    }

    async getMedrecconcile(db: Knex, hn: any) {
        const data = await db('medreconcile')
          .select(
            db.raw("'' as drug_hospcode"),
            'hospname as drug_hospname',
            'medname as drug_name',
            'meduse as drug_use',
            'lastdate as drug_receive_date'
          )
          .where('hn', hn);
      
        return data;
      }

    async getServicesCoc_old(db: Knex, hn: any) {
        let data = await db.raw(`
        SELECT o.hn,
        o.vn as seq,p.pname as title_name,p.fname as first_name,p.lname as last_name, 
		DATE_FORMAT(date(o.vstdttm),'%Y-%m-%d') as date_serv, 
        DATE_FORMAT(time(o.vstdttm),'%h:%i:%s') as time_serv, 
        c.namecln as department
        FROM ovst as o 
        LEFT JOIN cln as c ON c.cln = o.cln 
        INNER JOIN pt as p	ON p.hn = o.hn
        WHERE o.hn ='${hn}' and o.an = '0'
		UNION
		SELECT o.hn,
		o.vn as seq,p.pname as title_name,p.fname as first_name,p.lname as last_name, 
		DATE_FORMAT(date(o.vstdttm),'%Y-%m-%d') as date_serv, 
        DATE_FORMAT(time(o.vstdttm),'%h:%i:%s') as time_serv, 
        idpm.nameidpm as department
		from ipt 
		LEFT JOIN idpm  on idpm.idpm = ipt.ward
		INNER JOIN ovst as o on o.vn = ipt.vn
	    INNER JOIN pt as p	ON p.hn = o.hn
        WHERE ipt.hn ='${hn}'
        ORDER BY seq DESC limit 3
        `);
        return data[0];
    }

    async getServicesCoc(db: Knex, hn: any) {
        const data = await db
          .select(
            'o.hn',
            'o.vn as seq',
            'p.pname as title_name',
            'p.fname as first_name',
            'p.lname as last_name',
            db.raw("DATE_FORMAT(date(o.vstdttm),'%Y-%m-%d') as date_serv"),
            db.raw("DATE_FORMAT(time(o.vstdttm),'%h:%i:%s') as time_serv"),
            'c.namecln as department'
          )
          .from('ovst as o')
          .leftJoin('cln as c', 'c.cln', 'o.cln')
          .innerJoin('pt as p', 'p.hn', 'o.hn')
          .where('o.hn', hn)
          .andWhere('o.an', '0')
          .union(function() {
            this.select(
              'o.hn',
              'o.vn as seq',
              'p.pname as title_name',
              'p.fname as first_name',
              'p.lname as last_name',
              db.raw("DATE_FORMAT(date(o.vstdttm),'%Y-%m-%d') as date_serv"),
              db.raw("DATE_FORMAT(time(o.vstdttm),'%h:%i:%s') as time_serv"),
              'idpm.nameidpm as department'
            )
            .from('ipt')
            .leftJoin('idpm', 'idpm.idpm', 'ipt.ward')
            .innerJoin('ovst as o', 'o.vn', 'ipt.vn')
            .innerJoin('pt as p', 'p.hn', 'o.hn')
            .where('ipt.hn', hn);
          })
          .orderBy('seq', 'desc')
          .limit(3);
      
        return data;
      }

    async getProfileCoc_old(db: Knex, hn: any) {
        let data = await db.raw(`
        select p.hn as hn, p.pop_id as cid, 
        if(p.pname = '',
        cast(
            (case p.male 
                when 1 then if(p.mrtlst < 6,
                    if(timestampdiff(YEAR,p.brthdate,now()) < 15,'ด.ช.','นาย'),
                    if((timestampdiff(YEAR,p.brthdate,now()) < 20),'เณร','พระ')) 
                when 2 then if((p.mrtlst = 1),
                    if((timestampdiff(YEAR,p.brthdate,now()) < 15),'ด.ญ.','น.ส.'),
                    if((p.mrtlst < 6),'นาง','แม่ชี')) 
            end) as char(8) charset utf8),
        convert(p.pname using utf8)) as title_name
        ,p.fname as first_name,p.lname as last_name
        ,p.moopart,p.addrpart,p.tmbpart,amppart,chwpart,p.brthdate
        ,concat(lpad(timestampdiff(year,p.brthdate,now()),3,'0'),'-'
        ,lpad(mod(timestampdiff(month,p.brthdate,now()),12),2,'0'),'-'
        ,lpad(if(DAYOFMONTH(p.brthdate)>DAYOFMONTH(now())
        ,day(LAST_DAY(SUBDATE(now(),INTERVAL 1 month)))-DAYOFMONTH(p.brthdate)+DAYOFMONTH(now())
        ,DAYOFMONTH(now())-DAYOFMONTH(p.brthdate)),2,'0')) as age
        ,if(p.male = 1,'ชาย','หญิง') as sex
        ,m.namemale as sexname,j.nameoccptn as occupation
        ,p.pttype as pttype_id,t.namepttype as pttype_name,s.card_id as pttype_no,s.hospmain
        ,c.hosname as hospmain_name,s.hospsub,h.hosname as hospsub_name,p.fdate as registdate,p.ldate as visitdate
        ,p.fthname as father_name,p.mthname as mother_name,p.couple as couple_name,p.infmname as contact_name,p.statusinfo as contact_relation,p.infmtel as contact_mobile
        FROM pt as p 
        INNER JOIN pttype as t on p.pttype=t.pttype
        left join male as m on p.male=m.male
        left join occptn as j on p.occptn=j.occptn
        left join insure as s on p.hn=s.hn and p.pttype=s.pttype
        left join chospital as c on s.hospmain=c.hoscode
        left join chospital as h on s.hospmain=h.hoscode
        WHERE p.hn ='${hn}'
        `);
        return data[0];
    }


    async getProfileCoc(db: Knex, hn: any) {
        const data = await db
          .select(
            'p.hn as hn',
            'p.pop_id as cid',
            db.raw(`
              IF(p.pname = '',
                CAST(
                  (CASE p.male 
                    WHEN 1 THEN 
                      IF(p.mrtlst < 6,
                        IF(TIMESTAMPDIFF(YEAR, p.brthdate, NOW()) < 15, 'ด.ช.', 'นาย'),
                        IF(TIMESTAMPDIFF(YEAR, p.brthdate, NOW()) < 20, 'เณร', 'พระ')) 
                    WHEN 2 THEN 
                      IF(p.mrtlst = 1, 
                        IF(TIMESTAMPDIFF(YEAR, p.brthdate, NOW()) < 15, 'ด.ญ.', 'น.ส.'),
                        IF(p.mrtlst < 6, 'นาง', 'แม่ชี')) 
                  END) AS CHAR(8) CHARSET utf8
                ), 
                CONVERT(p.pname USING utf8)
              ) AS title_name`),
            'p.fname as first_name',
            'p.lname as last_name',
            'p.moopart',
            'p.addrpart',
            'p.tmbpart',
            'p.amppart',
            'p.chwpart',
            'p.brthdate',
            db.raw(`
              CONCAT(
                LPAD(TIMESTAMPDIFF(YEAR, p.brthdate, NOW()), 3, '0'), '-', 
                LPAD(MOD(TIMESTAMPDIFF(MONTH, p.brthdate, NOW()), 12), 2, '0'), '-', 
                LPAD(
                  IF(DAYOFMONTH(p.brthdate) > DAYOFMONTH(NOW()), 
                    DAY(LAST_DAY(SUBDATE(NOW(), INTERVAL 1 MONTH))) - DAYOFMONTH(p.brthdate) + DAYOFMONTH(NOW()), 
                    DAYOFMONTH(NOW()) - DAYOFMONTH(p.brthdate)
                  ), 2, '0'
                )
              ) AS age`),
            db.raw(`IF(p.male = 1, 'ชาย', 'หญิง') AS sex`),
            'm.namemale as sexname',
            'j.nameoccptn as occupation',
            'p.pttype as pttype_id',
            't.namepttype as pttype_name',
            's.card_id as pttype_no',
            's.hospmain',
            'c.hosname as hospmain_name',
            's.hospsub',
            'h.hosname as hospsub_name',
            'p.fdate as registdate',
            'p.ldate as visitdate',
            'p.fthname as father_name',
            'p.mthname as mother_name',
            'p.couple as couple_name',
            'p.infmname as contact_name',
            'p.statusinfo as contact_relation',
            'p.infmtel as contact_mobile'
          )
          .from('pt as p')
          .innerJoin('pttype as t', 'p.pttype', 't.pttype')
          .leftJoin('male as m', 'p.male', 'm.male')
          .leftJoin('occptn as j', 'p.occptn', 'j.occptn')
          .leftJoin('insure as s', function() {
            this.on('p.hn', '=', 's.hn').andOn('p.pttype', '=', 's.pttype');
          })
          .leftJoin('chospital as c', 's.hospmain', 'c.hoscode')
          .leftJoin('chospital as h', 's.hospmain', 'h.hoscode')
          .where('p.hn', hn);
      
        return data;
      }
}

