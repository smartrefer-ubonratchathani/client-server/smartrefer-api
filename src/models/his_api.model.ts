const request = require("request");
const urlApi = process.env.DB_HOST;
const token = process.env.DB_PASSWORD;
import * as Knex from "knex";

export class ApiModel {

    async getLogin(db: Knex, username: any, password: any) {
        const options = {
            method: 'POST',
            url: `${urlApi}/auth/getLogin`,
            headers: { 'content-type': 'application/json' },
            body: { username: username, password: password },
            json: true
        };
        return new Promise((resolve, reject) => {
            request(options, function (error: any, response: any, body: any) {
                if (error) {
                    reject(error);
                } else {
                    resolve(body);
                }
            });
        });
    }

    async getServices(db: Knex, hn: any, seq: any){
        const options = {
            method: 'POST',
            url: `${urlApi}/refer/getServices`,
            headers: { 
                'cache-control': 'no-cache',
                'content-type': 'application/json',
                'authorization': `Bearer ${token}`,
            },
            body: { hn: hn, seq: seq },
            json: true
        };
        return new Promise((resolve, reject) => {
            request(options, function (error: any, response: any, body: any) {
                if (error) {
                    reject(error);
                } else {
                    resolve(body);
                }
            });
        });
    }

    async getProfile(db: Knex, hn: any, seq: any){
        const options = {
            method: 'POST',
            url: `${urlApi}/refer/getProfile`,
            headers: { 
                'cache-control': 'no-cache',
                'content-type': 'application/json',
                'authorization': `Bearer ${token}`,
            },
            body: { hn: hn },
            json: true
        };
        return new Promise((resolve, reject) => {
            request(options, function (error: any, response: any, body: any) {
                if (error) {
                    reject(error);
                } else {
                    resolve(body);
                }
            });
        });
    }

    async getHospital(db: Knex, hn: any){
        const options = {
            method: 'POST',
            url: `${urlApi}/refer/getHospital`,
            headers: { 
                'cache-control': 'no-cache',
                'content-type': 'application/json',
                'authorization': `Bearer ${token}`,
            },
            json: true
        };
        return new Promise((resolve, reject) => {
            request(options, function (error: any, response: any, body: any) {
                if (error) {
                    reject(error);
                } else {
                    resolve(body);
                }
            });
        });
    }

    async getAllergyDetail(db: Knex, hn: any){
        const options = {
            method: 'POST',
            url: `${urlApi}/refer/getAllergyDetail`,
            headers: { 
                'cache-control': 'no-cache',
                'content-type': 'application/json',
                'authorization': `Bearer ${token}`,
            },
            body: { hn: hn },
            json: true
        };
        return new Promise((resolve, reject) => {
            request(options, function (error: any, response: any, body: any) {
                if (error) {
                    reject(error);
                } else {
                    resolve(body);
                }
            });
        });
    }

    async getChronic(db: Knex, hn: any){
        const options = {
            method: 'POST',
            url: `${urlApi}/refer/getChronic`,
            headers: { 
                'cache-control': 'no-cache',
                'content-type': 'application/json',
                'authorization': `Bearer ${token}`,
            },
            body: { hn: hn },
            json: true
        };
        return new Promise((resolve, reject) => {
            request(options, function (error: any, response: any, body: any) {
                if (error) {
                    reject(error);
                } else {
                    resolve(body);
                }
            });
        });
    }

    async getDiagnosis(db: Knex, hn: any, dateServe: any, seq: any){
        const options = {
            method: 'POST',
            url: `${urlApi}/refer/getDiagnosis`,
            headers: { 
                'cache-control': 'no-cache',
                'content-type': 'application/json',
                'authorization': `Bearer ${token}`,
            },
            body: { hn: hn, dateServe: dateServe, seq: seq },
            json: true
        };
        return new Promise((resolve, reject) => {
            request(options, function (error: any, response: any, body: any) {
                if (error) {
                    reject(error);
                } else {
                    resolve(body);
                }
            });
        });
    }

    async getRefer(db: Knex, hn: any, dateServe: any, seq: any, referno: any){
        const options = {
            method: 'POST',
            url: `${urlApi}/refer/getRefer`,
            headers: { 
                'cache-control': 'no-cache',
                'content-type': 'application/json',
                'authorization': `Bearer ${token}`,
            },
            body: { hn: hn, dateServe: dateServe, seq: seq, referno: referno },
            json: true
        };
        return new Promise((resolve, reject) => {
            request(options, function (error: any, response: any, body: any) {
                if (error) {
                    reject(error);
                } else {
                    resolve(body);
                }
            });
        });
    }

    async getDrugs(db: Knex, hn: any, dateServe: any, seq: any){
        const options = {
            method: 'POST',
            url: `${urlApi}/refer/getDrugs`,
            headers: { 
                'cache-control': 'no-cache',
                'content-type': 'application/json',
                'authorization': `Bearer ${token}`,
            },
            body: { hn: hn, dateServe: dateServe, seq: seq },
            json: true
        };
        return new Promise((resolve, reject) => {
            request(options, function (error: any, response: any, body: any) {
                // console.log(body);
                if (error) {
                    reject(error);
                } else {
                    resolve(body);
                }
            });
        });
    }

    async getLabs(db: Knex, hn: any, dateServe: any, seq: any){
        const options = {
            method: 'POST',
            url: `${urlApi}/refer/getLabs`,
            headers: { 
                'cache-control': 'no-cache',
                'content-type': 'application/json',
                'authorization': `Bearer ${token}`,
            },
            body: { hn: hn, dateServe: dateServe, seq: seq },
            json: true
        };
        return new Promise((resolve, reject) => {
            request(options, function (error: any, response: any, body: any) {

                if (error) {
                    reject(error);
                } else {
                    resolve(body);
                }
            });
        });
    }

    async getAppointment(db: Knex, hn: any, dateServe: any, seq: any){
        const options = {
            method: 'POST',
            url: `${urlApi}/refer/getAppointment`,
            headers: { 
                'cache-control': 'no-cache',
                'content-type': 'application/json',
                'authorization': `Bearer ${token}`,
            },
            body: { hn: hn, dateServe: dateServe, seq: seq },
            json: true
        };
        // console.log(options);
        return new Promise((resolve, reject) => {
            request(options, function (error: any, response: any, body: any) {
                // console.log(body);
                if (error) {
                    reject(error);
                } else {
                    resolve(body);
                }
            });
        });
    }

    async getVaccine(db: Knex, hn: any) {
        const options = {
            method: 'POST',
            url: `${urlApi}/refer/getVaccine`,
            headers: {
                'cache-control': 'no-cache',
                'content-type': 'application/json',
                'authorization': `Bearer ${token}`,
            },
            body: { hn: hn },
            json: true
        };
        // console.log(options);
        return new Promise((resolve, reject) => {
            request(options, function (error: any, response: any, body: any) {
                // console.log(body);
                if (error) {
                    reject(error);
                } else {
                    resolve(body);
                }
            });
        });
    }

    async getProcedure(db: Knex, hn: any, dateServe: any, seq: any) {
        const options = {
            method: 'POST',
            url: `${urlApi}/refer/getProcedure`,
            headers: {
                'cache-control': 'no-cache',
                'content-type': 'application/json',
                'authorization': `Bearer ${token}`,
            },
            body: { hn: hn, dateServe: dateServe, seq: seq },
            json: true
        };
        // console.log(options);
        return new Promise((resolve, reject) => {
            request(options, function (error: any, response: any, body: any) {
                // console.log(body);
                if (error) {
                    reject(error);
                } else {
                    resolve(body);
                }
            });
        });
    }

    async getNurture(db: Knex, hn: any, dateServe: any, seq: any) {
        const options = {
            method: 'POST',
            url: `${urlApi}/refer/getNurture`,
            headers: {
                'cache-control': 'no-cache',
                'content-type': 'application/json',
                'authorization': `Bearer ${token}`,
            },
            body: { hn: hn, dateServe: dateServe, seq: seq },
            json: true
        };
        // console.log(options);
        return new Promise((resolve, reject) => {
            request(options, function (error: any, response: any, body: any) {
                // console.log(body);
                if (error) {
                    reject(error);
                } else {
                    resolve(body);
                }
            });
        });
    }

    async getPhysical(db: Knex, hn: any, dateServe: any, seq: any) {
        const options = {
            method: 'POST',
            url: `${urlApi}/refer/getPhysical`,
            headers: {
                'cache-control': 'no-cache',
                'content-type': 'application/json',
                'authorization': `Bearer ${token}`,
            },
            body: { hn: hn, dateServe: dateServe, seq: seq },
            json: true
        };
        // console.log(options);
        return new Promise((resolve, reject) => {
            request(options, function (error: any, response: any, body: any) {
                // console.log(body);
                if (error) {
                    reject(error);
                } else {
                    resolve(body);
                }
            });
        });
    }

    async getPillness(db: Knex, hn: any, dateServe: any, seq: any) {
        const options = {
            method: 'POST',
            url: `${urlApi}/refer/getPillness`,
            headers: {
                'cache-control': 'no-cache',
                'content-type': 'application/json',
                'authorization': `Bearer ${token}`,
            },
            body: { hn: hn, dateServe: dateServe, seq: seq },
            json: true
        };
        // console.log(options);
        return new Promise((resolve, reject) => {
            request(options, function (error: any, response: any, body: any) {
                // console.log(body);
                if (error) {
                    reject(error);
                } else {
                    resolve(body);
                }
            });
        });
    }

    async getBedsharing(db: Knex) {
        const options = {
            method: 'POST',
            url: `${urlApi}/refer/getBedsharing`,
            headers: {
                'cache-control': 'no-cache',
                'content-type': 'application/json',
                'authorization': `Bearer ${token}`,
            },
            json: true
        };
        // console.log(options);
        return new Promise((resolve, reject) => {
            request(options, function (error: any, response: any, body: any) {
                // console.log(body);
                if (error) {
                    reject(error);
                } else {
                    resolve(body);
                }
            });
        });
    }

    async getReferOut(db: Knex, start_date: any, end_date: any) {
        const options = {
            method: 'POST',
            url: `${urlApi}/refer/getReferOut`,
            headers: {
                'cache-control': 'no-cache',
                'content-type': 'application/json',
                'authorization': `Bearer ${token}`,
            },
            body: { start_date: start_date, end_date: end_date },
            json: true
        };
        // console.log(options);
        return new Promise((resolve, reject) => {
            request(options, function (error: any, response: any, body: any) {

                if (error) {
                    reject(error);
                } else {
                    resolve(body);
                }
            });
        });
    }

    async getReferBack(db: Knex, start_date: any, end_date: any) {
        const options = {
            method: 'POST',
            url: `${urlApi}/refer/getReferBack`,
            headers: {
                'cache-control': 'no-cache',
                'content-type': 'application/json',
                'authorization': `Bearer ${token}`,
            },
            body: { start_date: start_date, end_date: end_date },
            json: true
        };
        // console.log(options);
        return new Promise((resolve, reject) => {
            request(options, function (error: any, response: any, body: any) {

                if (error) {
                    reject(error);
                } else {
                    resolve(body);
                }
            });
        });
    }

    async getAppoint(db: Knex, hn: any, app_date: any) {
        const options = {
            method: 'POST',
            url: `${urlApi}/refer/getAppoint`,
            headers: {
                'cache-control': 'no-cache',
                'content-type': 'application/json',
                'authorization': `Bearer ${token}`,
            },
            body: { hn: hn, app_date: app_date },
            json: true
        };
        // console.log(options);
        return new Promise((resolve, reject) => {
            request(options, function (error: any, response: any, body: any) {

                if (error) {
                    reject(error);
                } else {
                    resolve(body);
                }
            });
        });
    }

    async getXray(db: Knex, hn: any, dateServe: any, seq: any) {
        const options = {
            method: 'POST',
            url: `${urlApi}/refer/getXray`,
            headers: {
                'cache-control': 'no-cache',
                'content-type': 'application/json',
                'authorization': `Bearer ${token}`,
            },
            body: {
                hn: hn,
                dateServe: dateServe,
                seq: seq
            },
            json: true
        };
        // console.log(options);
        return new Promise((resolve, reject) => {
            request(options, function (error: any, response: any, body: any) {

                if (error) {
                    reject(error);
                } else {
                    resolve(body);
                }
            });
        });
    }

    async getDepartment(db: Knex) {
        const options = {
            method: 'POST',
            url: `${urlApi}/refer/getDepartment`,
            headers: {
                'cache-control': 'no-cache',
                'content-type': 'application/json',
                'authorization': `Bearer ${token}`
            },
            json: true
        };
        // console.log(options);
        return new Promise((resolve, reject) => {
            request(options, function (error: any, response: any, body: any) {

                if (error) {
                    reject(error);
                } else {
                    resolve(body);
                }
            });
        });
    }

    async getPtHN(db: Knex, cid: any) {
        const options = {
            method: 'POST',
            url: `${urlApi}/refer/getPtHN`,
            headers: {
                'cache-control': 'no-cache',
                'content-type': 'application/json',
                'authorization': `Bearer ${token}`
            },
            body: { cid: cid },
            json: true
        };
        // console.log(options);
        return new Promise((resolve, reject) => {
            request(options, function (error: any, response: any, body: any) {

                if (error) {
                    reject(error);
                } else {
                    resolve(body);
                }
            });
        });
    }

    async getMedrecconcile(db: Knex, hn: any) {
        const options = {
            method: 'POST',
            url: `${urlApi}/refer/getMedrecconcile`,
            headers: {
                'cache-control': 'no-cache',
                'content-type': 'application/json',
                'authorization': `Bearer ${token}`
            },
            body: { hn: hn },
            json: true
        };
        // console.log(options);
        return new Promise((resolve, reject) => {
            request(options, function (error: any, response: any, body: any) {

                if (error) {
                    reject(error);
                } else {
                    resolve(body);
                }
            });
        });
    }

    async getServicesCoc(db: Knex, hn: any) {
        const options = {
            method: 'POST',
            url: `${urlApi}/refer/getServicesCoc`,
            headers: {
                'cache-control': 'no-cache',
                'content-type': 'application/json',
                'authorization': `Bearer ${token}`
            },
            body: { hn: hn },
            json: true
        };
        // console.log(options);
        return new Promise((resolve, reject) => {
            request(options, function (error: any, response: any, body: any) {

                if (error) {
                    reject(error);
                } else {
                    resolve(body);
                }
            });
        });
    }

    async getProfileCoc(db: Knex, hn: any) {
        const options = {
            method: 'POST',
            url: `${urlApi}/refer/getProfileCoc`,
            headers: {
                'cache-control': 'no-cache',
                'content-type': 'application/json',
                'authorization': `Bearer ${token}`
            },
            body: { hn: hn },
            json: true
        };
        // console.log(options);
        return new Promise((resolve, reject) => {
            request(options, function (error: any, response: any, body: any) {

                if (error) {
                    reject(error);
                } else {
                    // console.log(body);
                    resolve(body);
                }
            });
        });
    }

}