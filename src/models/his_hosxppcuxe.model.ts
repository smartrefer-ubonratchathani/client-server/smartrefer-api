import Knex = require('knex');
const hospcode = process.env.HIS_CODE;
var md5 = require('md5');

export class HisHosxpPcuXEModel {

  //ทดสอดแล้วผ่าน BY RENFIX
  async getLogin(db: Knex, username: any, password: any) {
   let pass = md5(password).toUpperCase();
    let data =  await db('opduser as o')
        .select('o.loginname as username', 'o.name as fullname')
        .select(db('opdconfig').first('hospitalcode').as('hcode').limit(1)) 
        .where('o.loginname', username)
        .andWhere('o.passweb', pass);
        return data;
  }

  //ทดสอดแล้วผ่าน BY RENFIX
  async getServices(db:Knex, hn:any, seq:any, referno:any) {
  let data = await db.raw(`
      SELECT
        o.vn as seq,
        p.pname as title_name,
        p.fname as first_name,
        p.lname as last_name,
        DATE_FORMAT(date(o.vstdate), '%Y-%m-%d') as date_serv,
        DATE_FORMAT(time(o.vsttime), '%h:%i:%s') as time_serv,
        c.department as department
      FROM ovst as o
      INNER JOIN kskdepartment as c ON c.depcode = o.main_dep
      INNER JOIN patient as p ON p.hn = o.hn
      WHERE o.hn = ? AND o.vn = ?
      UNION
      SELECT
        an.an as seq,
        p.pname as title_name,
        p.fname as first_name,
        p.lname as last_name,
        DATE_FORMAT(date(i.regdate), '%Y-%m-%d') as date_serv,
        DATE_FORMAT(time(i.regtime), '%h:%i:%s') as time_serv,
        w.name as department
      FROM an_stat as an
      INNER JOIN ward as w ON w.ward = an.ward
      INNER JOIN patient as p ON p.hn = an.hn
      INNER JOIN ipt as i ON i.an = an.an
      WHERE an.hn = ? AND an.an = ?
    `, [hn, seq, hn, seq]);
  
    return data[0];
  }

  //ทดสอดแล้วผ่าน BY RENFIX
  async getProfile(db: Knex, hn: any, seq: any, referno: any) {
  let data =await db('patient as p')
  .select(
    'p.hn as hn',
    'p.cid as cid',
    'p.pname as title_name',
    'p.fname as first_name',
    'p.lname as last_name',
    's.name as sex',
    'oc.name as occupation',
    'p.moopart',
    'p.addrpart',
    'p.tmbpart',
    'p.amppart',
    'p.chwpart',
    'p.birthday as brthdate',
    db.raw(`concat(lpad(timestampdiff(year,p.birthday,now()),3,'0'),'-',
      lpad(mod(timestampdiff(month,p.birthday,now()),12),2,'0'),'-',
      lpad(if(day(p.birthday)>day(now()),dayofmonth(now())-(day(p.birthday)-day(now())),day(now())-day(p.birthday)),2,'0')) as age`),
    'o.pttype as pttype_id',
    't.name as pttype_name',
    'o.pttypeno as pttype_no',
    'o.hospmain',
    'c.name as hospmain_name',
    'o.hospsub',
    'h.name as hospsub_name',
    'p.firstday as registdate',
    'p.last_visit as visitdate',
    'p.fathername as father_name',
    'p.mathername as mother_name',
    'p.informrelation as contact_name',
    'p.informtel as contact_mobile',
    'oc.name as occupation'
  )
  .innerJoin('ovst as o', 'o.hn', 'p.hn')
  .leftJoin('pttype as t', 'o.pttype', 't.pttype')
  .leftJoin('hospcode as c', 'o.hospmain', 'c.hospcode')
  .leftJoin('hospcode as h', 'o.hospsub', 'h.hospcode')
  .leftJoin('sex as s', 's.code', 'p.sex')
  .leftJoin('occupation as oc', 'oc.occupation', 'p.occupation')
  .where('p.hn', hn)
  .orderBy('o.vstdate', 'desc')
  .limit(1);
    return data;
  }

  //ทดสอดแล้วผ่าน BY RENFIX
  async getHospital(db: Knex, hn: any) {
    let data = await db.raw(`
      SELECT hospitalcode as provider_code,
             hospitalname as provider_name 
      from opdconfig limit 1
     `);
    return data[0];
  }

  //ทดสอดแล้วผ่าน BY RENFIX
  async getAllergyDetail(db: Knex, hn: any, referno: any) {
    let data = await db.raw(`
    SELECT agent as drug_name, 
           symptom as symptom ,
           begin_date as begin_date,
           date(entry_datetime) as daterecord
    FROM opd_allergy
    WHERE hn ='${hn}'
    `);
    return data[0];
  }

  async getChronic(db: Knex, hn: any, referno: any) {
    let data = await db.raw(`
    SELECT c.icd10 as icd_code,
           cm.regdate as start_date, 
           IF(i.tname!='', i.tname, "-") as icd_name
    FROM clinicmember as cm 
    inner join clinic c on cm.clinic = c.clinic
    INNER JOIN icd101 as i on i.code = c.icd10
    WHERE cm.hn ='${hn}'
    `);
    return data[0];
  }
 
  async getDiagnosis(db: Knex, hn: any, dateServe: any, seq: any, referno: any) {
    let data = await db.raw(`
      SELECT 
          o.vn AS seq,
          DATE_FORMAT(o.vstdate, '%Y-%m-%d') AS date_serv,
          TIME(o.vsttime) AS time_serv,
          IF((d.icd10 = '' OR d.icd10 IS NULL), 
              (SELECT ro.pdx FROM referout ro WHERE ro.vn = o.vn AND d.vn = '${seq}' LIMIT 1), 
              d.icd10) AS icd_code,
          i.name AS icd_name,
          d.diagtype AS diag_type, 
          IF((o.diag_text = '' OR o.diag_text IS NULL), 
              (SELECT ro.pre_diagnosis FROM referout ro WHERE ro.vn = o.vn AND d.vn = '${seq}' LIMIT 1), 
              o.diag_text) AS DiagNote,       
          '' AS diagtype_id          
      FROM ovst AS o
      INNER JOIN vn_stat v ON v.vn = o.vn
      INNER JOIN ovstdiag d ON d.vn = o.vn
      INNER JOIN icd101 AS i ON i.code = d.icd10
      LEFT JOIN referout AS ro ON ro.vn = o.vn
      WHERE d.vn = '${seq}' 
      AND (o.an = '' OR o.an IS NULL)
      UNION
      SELECT  
          i.an AS seq, 
          DATE_FORMAT(i.dchdate, '%Y-%m-%d') AS date_serv, 
          TIME(i.dchtime) AS time_serv,
          r.icd10 AS icd_code,
          c.name AS icd_name,
          r.diagtype AS diag_type, 
          c.name AS DiagNote,           
          r.diagtype AS diagtype_id          
      FROM ipt i
      INNER JOIN (
          SELECT an, icd10, diagtype, 'ipdx' AS f FROM iptdiag id WHERE id.an = '${seq}'
          UNION 
          SELECT vn AS an, pdx AS icd10, '1' AS diagtype, 'referdx' AS f FROM referout ro WHERE ro.vn = '${seq}'
      ) AS r ON r.an = i.an
      INNER JOIN icd101 AS c ON r.icd10 = c.code
      WHERE IF((SELECT COUNT(an) AS total FROM (
          SELECT an, icd10, diagtype, 'ipdx' AS f FROM iptdiag id WHERE id.an = '${seq}'
          UNION 
          SELECT vn AS an, pdx AS icd10, '1' AS diagtype, 'referdx' AS f FROM referout ro WHERE ro.vn = '${seq}'
      ) AS r) > 1, r.f = 'ipdx', r.f = 'referdx')
  `);
    return data[0];
  }

  async getRefer(db: Knex, hn: any, dateServe: any, seq: any, referno: any) {
    let data = await db.raw(`
    SELECT a.seq,a.an,a.pid,a.hn,a.referno,a.referdate,a.to_hcode,a.pttype_id,a.strength_id,a.strength_name,a.location_name,a.station_name,a.to_hcode_name,a.refer_cause,a.refertime,a.doctor,a.doctor_name
    FROM (
    select if(LENGTH(r.vn) >=  9,r.vn,o2.vn) as seq,
    o2.an as an,
    r.hn as pid,
    r.hn as hn,
    cast(replace(r.refer_number,"/",'-') as char(20) CHARACTER SET UTF8) as referno,
    r.refer_date as referdate,
    r.refer_hospcode as to_hcode,
    if(r.referout_emergency_type_id='3',1,2) as pttype_id,
    r.referout_emergency_type_id as strength_id,
    (case r.refer_cause
    when 1 then 'Life threatening'
    when 2 then 'Emergency'
    when 3 then 'Urgent'
    when 4 then 'Acute'
    when 5 then 'Non acute'
    else '' end
    ) as strength_name,
    r.refer_point as location_name,
    r.refer_point as station_name,
    (select name from hospcode where hospcode = refer_hospcode) as to_hcode_name,
    (select name from refer_cause where id = refer_cause) as refer_cause,
    refer_time as refertime,
    (select concat(licenseno,',',cid)  from doctor  where code = r.doctor) as doctor,
    (select name  from doctor where code = r.doctor) as doctor_name
    from referout as r
    left outer join ovst o on r.vn = o.vn
    left outer join ovst o2 on r.vn = o2.an
    where cast(replace(r.refer_number,"/",'-') as char(20) CHARACTER SET UTF8) = '${referno}'
    AND (if(LENGTH(r.vn) >=  9,r.vn,o2.vn))  = '${seq}'
    UNION
    select if(o.an is null,r.vn,o.an) as seq,
    o2.an as an,
    o.hn as pid,
    o.hn as hn,
    cast(replace(r.doc_no,"/",'-') as char(20) CHARACTER SET UTF8) as referno,
      DATE(r.reply_date_time) as referdate,
    r.dest_hospcode as to_hcode,
    "" as pttype_id,
    "" as strength_id,
    "" as strength_name,
    "" as location_name,
    "" as station_name,
    (select name from hospcode where hospcode = r.dest_hospcode) as to_hcode_name,
    ""as refer_cause,
      time(r.reply_date_time) as refertime,
    (select concat(licenseno,',',cid)  from doctor where (code = o.doctor or code = o2.doctor)) as doctor,
    (select name  from doctor where (code = o.doctor or code = o2.doctor)) as doctor_name
    from refer_reply as r
    left outer join ovst o on r.vn = o.vn
    left outer join ovst o2 on r.vn = o2.vn
    where cast(replace(r.doc_no,"/",'-') as char(20) CHARACTER SET UTF8) = '${referno}'
    AND (if(o.an is null,r.vn,o.an))  = '${seq}'
    ) a
    WHERE a.seq is not NULL
    `);
    return data[0];
  }

  async getDrugs(db: Knex, hn: any, dateServe: any, seq: any, referno: any) {
    let data = await db.raw(`
      SELECT a.seq, a.date_serv, a.time_serv, 
             REPLACE(a.drug_name, "\r\n", " ") AS drug_name, 
             a.qty, a.unit, 
             REPLACE(a.usage_line1, "\r\n", " ") AS usage_line1, 
             REPLACE(a.usage_line2, "\r\n", " ") AS usage_line2, 
             REPLACE(a.usage_line3, "\r\n", " ") AS usage_line3
      FROM (
          SELECT o.vn AS seq,
                 DATE_FORMAT(date(i.rxdate), '%Y%m%d') AS date_serv,
                 DATE_FORMAT(time(i.rxtime), '%h:%i:%s') AS time_serv,
                 d.name AS drug_name,
                 i.qty AS qty,
                 d.units AS unit,
                 IF((SELECT name1 FROM drugusage g WHERE g.drugusage = i.drugusage) != '', 
                    (SELECT name1 FROM drugusage g WHERE g.drugusage = i.drugusage), 
                    (SELECT s.name1 FROM sp_use s WHERE s.sp_use = i.sp_use)) AS usage_line1,
                 IF((SELECT name2 FROM drugusage g WHERE g.drugusage = i.drugusage) != '', 
                    (SELECT name2 FROM drugusage g WHERE g.drugusage = i.drugusage), 
                    (SELECT s.name2 FROM sp_use s WHERE s.sp_use = i.sp_use)) AS usage_line2,
                 IF((SELECT name3 FROM drugusage g WHERE g.drugusage = i.drugusage) != '', 
                    (SELECT name3 FROM drugusage g WHERE g.drugusage = i.drugusage), 
                    (SELECT s.name3 FROM sp_use s WHERE s.sp_use = i.sp_use)) AS usage_line3
          FROM ovst o
          LEFT OUTER JOIN opitemrece i ON o.vn = i.vn
          LEFT OUTER JOIN drugitems d ON i.icode = d.icode
          WHERE (o.an IS NULL OR o.an = '') 
            AND i.icode IN (SELECT icode FROM drugitems) 
            AND i.income IN ('03', '04') 
            AND i.vn = '${seq}'
          UNION
          SELECT o.vn AS seq,
                 DATE_FORMAT(date(i.rxdate), '%Y%m%d') AS date_serv,
                 DATE_FORMAT(time(i.rxtime), '%h:%i:%s') AS time_serv,
                 d.name AS drug_name,
                 i.qty AS qty,
                 d.units AS unit,
                 IF((SELECT name1 FROM drugusage g WHERE g.drugusage = i.drugusage) != '', 
                    (SELECT name1 FROM drugusage g WHERE g.drugusage = i.drugusage), 
                    (SELECT s.name1 FROM sp_use s WHERE s.sp_use = i.sp_use)) AS usage_line1,
                 IF((SELECT name2 FROM drugusage g WHERE g.drugusage = i.drugusage) != '', 
                    (SELECT name2 FROM drugusage g WHERE g.drugusage = i.drugusage), 
                    (SELECT s.name2 FROM sp_use s WHERE s.sp_use = i.sp_use)) AS usage_line2,
                 IF((SELECT name3 FROM drugusage g WHERE g.drugusage = i.drugusage) != '', 
                    (SELECT name3 FROM drugusage g WHERE g.drugusage = i.drugusage), 
                    (SELECT s.name3 FROM sp_use s WHERE s.sp_use = i.sp_use)) AS usage_line3
          FROM ovst o
          LEFT OUTER JOIN opitemrece i ON o.an = i.an
          LEFT OUTER JOIN drugitems d ON i.icode = d.icode
          WHERE (i.an IS NOT NULL AND i.vn IS NULL) 
            AND i.icode IN (SELECT icode FROM drugitems) 
            AND i.income IN ('03', '04') 
            AND o.vn = '${seq}'
      ) a
      WHERE a.seq IS NOT NULL
  `);
    return data[0];
  }

  async getLabs(db: Knex, hn: any, dateServe: any, seq: any, referno: any) {
    let data = await db.raw(`
      SELECT 
          receive_date AS date_serv,
          receive_time AS time_serv,
          form_name AS labgroup,
          lab_items_name_ref AS lab_name,
          REPLACE(lab_order_result, '\\r\\n', ' ') AS lab_result,
          li.lab_items_unit AS unit,
          lo.lab_items_normal_value_ref AS standard_result
      FROM ovst o
      INNER JOIN lab_head lh ON o.vn = lh.vn OR o.an = lh.vn
      INNER JOIN lab_order lo ON lh.lab_order_number = lo.lab_order_number
      INNER JOIN lab_items li ON li.lab_items_code = lo.lab_items_code
      WHERE lh.vn = ?
      AND lo.lab_order_result IS NOT NULL
  `, [seq]);
    return data[0];
  }

  async getAppointment(db: Knex, hn: any, dateServ: any, seq: any, referno: any) {
    let data = await db('oapp as o')
    .join('ovst', 'ovst.vn', 'o.vn')
    .select(
        'o.vn as seq',
        'o.vstdate as date_serv',
        'o.nextdate as date',
        'o.nexttime as time',
        db.raw('(SELECT name FROM clinic WHERE clinic = o.clinic) as department'),
        'o.app_cause as detail',
        db.raw('TIME(ovst.vsttime) as time_serv')
    )
    .where('o.vn', seq);
    return data[0];
  }

  //ทดสอดแล้วผ่าน BY RENFIX
  async getVaccine(db: Knex, hn: any, referno: any) {
    let data = await db('ovst_vaccine as ov')
    .leftJoin('person_vaccine as pv', 'ov.person_vaccine_id', 'pv.person_vaccine_id')
    .leftJoin('ovst as o', 'o.vn', 'ov.vn')
    .select(
        'o.vstdate as date_serv',
        'o.vsttime as time_serv',
        'pv.export_vaccine_code as vaccine_code',
        'pv.vaccine_name as vaccine_name'
    )
    .where('o.hn', hn);
    return data[0];
  }

  async getProcedure(db: Knex, hn: any, dateServe: any, seq: any, referno: any) {
    let data = await db.raw(`
      SELECT 
          o.hn AS pid,
          o.vn AS seq,
          o.vstdate AS date_serv,    
          o.vsttime AS time_serv,
          od.icd10 AS procedure_code,
          ic.name AS procedure_name,
          DATE_FORMAT(od.vstdate, '%Y%m%d') AS start_date,    
          DATE_FORMAT(od.vsttime, '%h:%i:%s') AS start_time,
          DATE_FORMAT(od.vstdate, '%Y%m%d') AS end_date,
          '00:00:00' AS end_time
      FROM ovst o
      LEFT JOIN ovstdiag od ON od.vn = o.vn
      INNER JOIN icd9cm1 ic ON ic.code = od.icd10 
      WHERE o.vn = '${seq}'
      GROUP BY o.vn, od.icd10
  
      UNION
  
      SELECT 
          o.hn AS pid,
          o.vn AS seq,
          o.vstdate AS date_serv,    
          o.vsttime AS time_serv,
          i.icd9 AS procedure_code,
          c.name AS procedure_name,
          DATE_FORMAT(i.opdate, '%Y%m%d') AS start_date,    
          DATE_FORMAT(i.optime, '%h:%i:%s') AS start_time,
          DATE_FORMAT(i.enddate, '%Y%m%d') AS end_date,
          '00:00:00' AS end_time
      FROM ovst o
      INNER JOIN iptoprt i ON i.an = o.an
      INNER JOIN icd101 c ON c.code = i.icd9 
      WHERE o.vn = '${seq}'
      GROUP BY o.vn, i.icd9
  `);
    return data[0];
  }

  async getNurture(db: Knex, hn: any, dateServe: any, seq: any, referno: any) {
    let data = await db.raw(`
   select o.vn as seq,o.hn,
           DATE_FORMAT(date(o.vstdate),'%Y-%m-%d') as date_serv,
           DATE_FORMAT(time(o.vsttime),'%h:%i:%s') as time_serv,
           p.bloodgrp as bloodgrp,
           s.bw as weight,
           s.height as height,
           s.bmi as bmi,
           s.temperature as temperature,
           s.pulse as pr,
           s.rr as rr,
           s.bps as sbp,
           s.bpd as dbp,
           REPLACE(s.cc,"\r\n"," ") as symptom,
           o.main_dep as depcode,
           k.department as department,
           n.gcs_m as movement_score,n.gcs_v as vocal_score,n.gcs_e as eye_score,n.o2sat as oxygen_sat,(n.gcs_e+n.gcs_v+n.gcs_m) as gak_coma_sco,
           if(
            (ovstdiag_text.provisional_dx_text="" or ovstdiag_text.provisional_dx_text is null)
            ,ro.pre_diagnosis
            ,ovstdiag_text.provisional_dx_text
            ) as diag_text,
      '' as pupil_right, 
      '' as pupil_left,
      REPLACE(ro.pmh,"\r\n"," ") as pmh
    FROM ovst as o
    LEFT JOIN opdscreen as s on o.vn = s.vn
    LEFT JOIN kskdepartment k ON k.depcode = o.main_dep
    INNER JOIN patient as p ON p.hn = o.hn
    LEFT JOIN referout as ro on ro.vn=o.vn
    LEFT JOIN  er_nursing_detail as n ON o.vn=n.vn
    left outer join ovstdiag_text on ovstdiag_text.vn=o.vn
    WHERE o.vn  = '${seq}'
    UNION
    select o.vn as seq,o.hn,
           DATE_FORMAT(date(o.regdate),'%Y-%m-%d') as date_serv,
           DATE_FORMAT(time(o.regtime),'%h:%i:%s') as time_serv,
           p.bloodgrp as bloodgrp,
           s.bw as weight,
           s.height as height,
           s.bmi as bmi,
           s.temperature as temperature,
           s.pulse as pr,
           s.rr as rr,
           s.bps as sbp,
           s.bpd as dbp,
           replace(replace(s.cc,char(13),' '),char(10),' ') as symptom,
           ""as depcode,
          "" as department,
           n.gcs_m as movement_score,n.gcs_v as vocal_score,n.gcs_e as eye_score,n.o2sat as oxygen_sat,(n.gcs_e+n.gcs_v+n.gcs_m) as gak_coma_sco,
           if((ovstdiag_text.provisional_dx_text="" or ovstdiag_text.provisional_dx_text is null),(select ro.pre_diagnosis from referout ro where ro.vn=o.vn and o.an ='${seq}' limit 1) ,ovstdiag_text.provisional_dx_text) as diag_text,
          '' as pupil_right,
           '' as pupil_left,
           REPLACE(ro.pmh,"\r\n"," ") as pmh
    FROM ipt as o
    LEFT JOIN opdscreen as s on o.vn = s.vn
    INNER JOIN patient as p ON p.hn = o.hn
    LEFT JOIN referout as ro on ro.vn=o.an
    LEFT JOIN  er_nursing_detail as n ON o.vn=n.vn
    left outer join ovstdiag_text on ovstdiag_text.vn=o.vn
    WHERE o.an  ='${seq}'
        `);
    return data[0];
  }

  async getPhysical(db: Knex, hn: any, dateServe: any, seq: any, referno: any) {
    let data = await db.raw(`
      SELECT v.seq, GROUP_CONCAT(v.pe) as pe
      FROM (
          SELECT o.vn as seq, REPLACE(o.pe, '\\r\\n', ' ') as pe 
          FROM opdscreen o 
          WHERE o.vn = ?
          UNION ALL
          SELECT i.vn as seq, REPLACE(d.note, '\\r\\n', ' ') as pe 
          FROM ipt_discharge_note d
          LEFT OUTER JOIN ipt i ON i.an = d.an
          WHERE i.vn = ?
      ) v
      GROUP BY seq
  `, [seq, seq]);
    return data[0];
  }

  async getPillness(db: Knex, hn: any, dateServe: any, seq: any, referno: any) {
    let data = await db.raw(`
      SELECT x.seq, REPLACE(GROUP_CONCAT(DISTINCT x.hpi), ",", " ") AS hpi 
      FROM (
          SELECT o.vn AS seq, REPLACE(o.hpi, "\r\n", " ") AS hpi 
          FROM referout AS o 
          WHERE o.vn = ?
          UNION
          SELECT o.vn AS seq, REPLACE(o.hpi, "\r\n", " ") AS hpi 
          FROM opdscreen AS o 
          WHERE o.vn = ?
          UNION
          SELECT o.vn AS seq, REPLACE(o.hpi_text, "\r\n", " ") AS hpi 
          FROM patient_history_hpi AS o 
          WHERE o.vn = ?
      ) AS x
  `, [seq, seq, seq]);
    return data[0];
  }

  async getXray(db: Knex, hn: any, dateServe: any, seq: any, referno: any) {
    let data = await db.raw(`
      SELECT x.request_date AS xray_date, i.xray_items_name AS xray_name
      FROM xray_report x
      LEFT OUTER JOIN xray_items i ON i.xray_items_code = x.xray_items_code  
      WHERE x.confirm = 'Y' AND x.confirm_read_film <> 'Y' AND x.vn = ?
      UNION
      SELECT x.request_date AS xray_date, i.xray_items_name AS xray_name
      FROM xray_report x
      LEFT OUTER JOIN xray_items i ON i.xray_items_code = x.xray_items_code  
      WHERE x.confirm = 'Y' AND x.an = ?
      GROUP BY x.xn
  `, [seq, seq]);
    return data[0];
  }

  //ทดสอดแล้วผ่าน BY RENFIX
  async getBedsharing(db: Knex) {
    let data = await db('bedno as b')
    .leftJoin('roomno as r', 'r.roomno', 'b.roomno')
    .leftJoin('ward as w', 'w.ward', 'r.ward')
    .leftJoin('iptadm as a', 'a.bedno', 'b.bedno')
    .leftJoin('ipt as i', function() {
      this.on('i.an', '=', 'a.an').andOnNull('i.dchdate');
    })
    .select(
      'w.ward as ward_code',
      'w.name as ward_name',
      db.raw('COUNT(i.an) as ward_pt'),
      db.raw('COUNT(DISTINCT b.bedno) as ward_bed'),
      'w.bedcount as ward_standard'
    )
    .whereNull('b.bed_status_type_id')
    .andWhere('w.ward_active', '<>', 'N')
    .groupBy('w.name');
    return data[0];
  }

  //ทดสอดแล้วผ่าน BY RENFIX
  async getReferOut(db: Knex, start_date: any, end_date: any) {
    let data = await db.raw(`
      SELECT 
        r.vn as seq,
        r.hn,
        o2.an as an,
        pt.pname as title_name,
        pt.fname as first_name,
        pt.lname as last_name,
        CAST(REPLACE(r.refer_number, "/", '-') AS CHAR(20) CHARACTER SET UTF8) as referno,
        r.refer_date as referdate,
        r.refer_hospcode as to_hcode,
        r.refer_point as location_name,
        IF(rt.referout_type_id = '1', '1', '2') as pttype_id,
        IF(rt.referout_type_id = '1', 'Non Trauma', 'Trauma') as typept_name,
        r.referout_emergency_type_id as strength_id,
        CASE r.referout_emergency_type_id
          WHEN 1 THEN 'Resucitate'
          WHEN 2 THEN 'Emergency'
          WHEN 3 THEN 'Urgency'
          WHEN 4 THEN 'Semi Urgency'
          WHEN 5 THEN 'Non Urgency'
          ELSE '' 
        END as strength_name,
        h.name as to_hcode_name,
        rr.name as refer_cause,
        TIME(r.refer_time) as refertime,
        d.licenseno as doctor,
        d.name as doctor_name
      FROM referout r
      INNER JOIN patient pt ON pt.hn = r.hn
      LEFT OUTER JOIN ovst o2 ON r.vn = o2.an
      LEFT OUTER JOIN referout_type rt ON rt.referout_type_id = r.referout_type_id
      LEFT OUTER JOIN doctor d ON d.code = r.doctor
      LEFT OUTER JOIN rfrcs rr ON rr.rfrcs = r.rfrcs
      LEFT OUTER JOIN hospcode h ON h.hospcode = r.refer_hospcode
      WHERE r.refer_date BETWEEN '${start_date}' AND '${end_date}'
        AND r.referout_type_id IN (1, 2, 3)
        AND r.refer_number NOT IN (
          SELECT refer_reply.doc_no 
          FROM refer_reply 
          WHERE refer_reply.doc_no IS NOT NULL 
            AND DATE(refer_reply.reply_date_time) BETWEEN '${start_date}' AND '${end_date}'
        )
      GROUP BY referno
    `);
    return data[0];
  }

  //ทดสอดแล้วผ่าน BY RENFIX
  async getReferBack(db: Knex, start_date: any, end_date: any) {
    let data = await db.raw(`
      SELECT 
          IF(o.an IS NULL, r.vn, o.an) AS seq,
          o.an AS an,
          pt.hn,
          pt.pname AS title_name,
          pt.fname AS first_name, 
          pt.lname AS last_name,
          CAST(REPLACE(r.doc_no, "/", '-') AS CHAR(20) CHARACTER SET UTF8) AS referno,
          DATE(r.reply_date_time) AS referdate,
          r.dest_hospcode AS to_hcode,
          h.name AS to_hcode_name,
          '' AS refer_cause,
          TIME(r.reply_date_time) AS refertime,
          d.licenseno AS doctor,
          d.name AS doctor_name
      FROM refer_reply r
      INNER JOIN ovst o ON o.vn = r.vn
      INNER JOIN patient pt ON pt.hn = o.hn
      LEFT OUTER JOIN hospcode h ON h.hospcode = r.dest_hospcode
      LEFT OUTER JOIN doctor d ON d.code = o.doctor
      WHERE DATE(r.reply_date_time) BETWEEN '${start_date}' AND '${end_date}'
  
      UNION
  
      SELECT
          CASE WHEN o2.an IS NULL THEN r.vn ELSE o2.an END AS seq,
          o2.an AS an,
          pt.hn,
          pt.pname AS title_name,
          pt.fname AS first_name,
          pt.lname AS last_name,
          CAST(REPLACE(r.refer_number, "/", '-') AS CHAR(20) CHARACTER SET UTF8) AS referno,
          r.refer_date AS referdate,
          r.refer_hospcode AS to_hcode,
          h.name AS to_hcode_name,
          '' AS refer_cause,
          r.refer_date AS refertime,
          d.licenseno AS doctor,
          d.name AS doctor_name 
      FROM referout r
      INNER JOIN patient pt ON pt.hn = r.hn
      LEFT OUTER JOIN ovst o2 ON r.vn = o2.an
      LEFT OUTER JOIN referout_type rt ON rt.referout_type_id = r.referout_type_id
      LEFT OUTER JOIN doctor d ON d.code = r.doctor
      LEFT OUTER JOIN rfrcs rr ON rr.rfrcs = r.rfrcs
      LEFT OUTER JOIN hospcode h ON h.hospcode = r.refer_hospcode
      WHERE r.refer_date BETWEEN '${start_date}' AND '${end_date}'  
      AND r.referout_type_id IN (4, 5, 6)
  `);
    return data[0];
  }

 //ทดสอดแล้วผ่าน BY RENFIX
  async getAppoint(db: Knex, hn: any, app_date: any, referno: any) {
    let data = await db('oapp as a')
    .leftJoin('clinic as c', 'c.clinic', 'a.clinic')
    .select(
        'a.vn as seq',
        'a.nextdate as receive_apppoint_date',
        'a.nexttime as receive_apppoint_beginttime',
        'a.endtime as receive_apppoint_endtime',
        'a.app_user as receive_apppoint_doctor',
        db.raw('GROUP_CONCAT(DISTINCT c.name) as receive_apppoint_clinic'),
        db.raw('GROUP_CONCAT(DISTINCT a.app_cause) as receive_text'),
        db.raw("'' as receive_by")
    )
    .where({
        'a.hn': hn,
        'a.vstdate': app_date
    });
    return data[0];
  }

   //ทดสอดแล้วผ่าน BY RENFIX
  async getDepartment(db: Knex) {
    let data = await db.raw(`
            select depcode as dep_code,department as dep_name from kskdepartment
        `);
    return data[0];
  }

   //ทดสอดแล้วผ่าน BY RENFIX
  async getPtHN(db: Knex, cid: any) {
    let data = await db.raw(`
            select hn from patient where cid = '${cid}'
        `);
    return data[0];
  }

  //ทดสอดแล้วผ่าน BY RENFIX
  async getMedrecconcile(db: Knex, hn: any) {
    let data = await db.raw(`
      SELECT 
          '' AS drug_hospcode,
          '' AS drug_hospname,
          CONCAT(s.name, ' ', s.strength, ' ', s.units) AS drug_name,
          IF(
              (SELECT CONCAT(d.name1, d.name2, d.name3) FROM drugusage d WHERE d.drugusage = o.drugusage) = '',
              (SELECT CONCAT(d.name1, d.name2, d.name3) FROM drugusage d WHERE d.drugusage = o.drugusage),
              (SELECT CONCAT(u.name1, u.name2, u.name3) FROM sp_use u WHERE u.sp_use = o.sp_use)
          ) AS drug_use,
          o.rxdate AS drug_receive_date
      FROM opitemrece o
      LEFT JOIN s_drugitems s ON s.icode = o.icode
      LEFT JOIN drugusage d ON d.drugusage = o.drugusage
      LEFT JOIN drugitems i ON i.icode = o.icode
      LEFT JOIN sp_use u ON u.sp_use = o.sp_use
      WHERE o.hn = '${hn}'
      AND o.income IN ('03', '04')
      AND o.rxdate >= DATE_ADD(CURDATE(), INTERVAL -3 MONTH)
      GROUP BY o.icode
      ORDER BY o.rxdate
  `);
    return data[0];
  }

  //ทดสอดแล้วผ่าน BY RENFIX
  async getServicesCoc(db: Knex, hn: any) {
    let data = await db.raw(`
      SELECT
          o.hn,
          o.vn as seq,
          p.pname as title_name,
          p.fname as first_name,
          p.lname as last_name,
          DATE_FORMAT(date(o.vstdate), '%Y-%m-%d') as date_serv,
          DATE_FORMAT(time(o.vsttime), '%h:%i:%s') as time_serv,
          c.department as department
      FROM ovst as o
      INNER JOIN kskdepartment as c ON c.depcode = o.main_dep
      INNER JOIN patient as p ON p.hn = o.hn
      WHERE o.hn = ?
      UNION
      SELECT
          an.hn,
          an.an as seq,
          p.pname as title_name,
          p.fname as first_name,
          p.lname as last_name,
          DATE_FORMAT(date(i.regdate), '%Y-%m-%d') as date_serv,
          DATE_FORMAT(time(i.regtime), '%h:%i:%s') as time_serv,
          w.name as department
      FROM an_stat as an
      INNER JOIN ward as w ON w.ward = an.ward
      INNER JOIN patient as p ON p.hn = an.hn
      INNER JOIN ipt as i ON i.an = an.an
      WHERE an.hn = ?
      ORDER BY seq DESC
      LIMIT 3
  `, [hn, hn]);
    return data[0];
  }

   //ทดสอดแล้วผ่าน BY RENFIX
  async getProfileCoc(db: Knex, hn: any) {
    let data = await db('patient as p')
    .select([
      'p.hn as hn',
      'p.cid as cid',
      'p.pname as title_name',
      'p.fname as first_name',
      'p.lname as last_name',
      's.name as sex',
      'oc.name as occupation',
      'p.moopart',
      'p.addrpart',
      'p.tmbpart',
      'p.amppart',
      'p.chwpart',
      'p.birthday as brthdate',
      db.raw(`
        CONCAT(
          LPAD(TIMESTAMPDIFF(YEAR, p.birthday, NOW()), 3, '0'), '-',
          LPAD(MOD(TIMESTAMPDIFF(MONTH, p.birthday, NOW()), 12), 2, '0'), '-',
          LPAD(
            IF(DAY(p.birthday) > DAY(NOW()), 
              DAYOFMONTH(NOW()) - (DAY(p.birthday) - DAY(NOW())), 
              DAY(NOW()) - DAY(p.birthday)
            ), 2, '0'
          )
        ) as age
      `),
      'o.pttype as pttype_id',
      't.name as pttype_name',
      'o.pttypeno as pttype_no',
      'o.hospmain',
      'c.name as hospmain_name',
      'o.hospsub',
      'h.name as hospsub_name',
      'p.firstday as registdate',
      'p.last_visit as visitdate',
      'p.fathername as father_name',
      'p.mathername as mother_name',
      'p.informrelation as contact_name',
      'p.informtel as contact_mobile',
      'oc.name as occupation'
    ])
    .innerJoin('ovst as o', 'o.hn', 'p.hn')
    .leftJoin('pttype as t', 'o.pttype', 't.pttype')
    .leftJoin('hospcode as c', 'o.hospmain', 'c.hospcode')
    .leftJoin('hospcode as h', 'o.hospsub', 'h.hospcode')
    .leftJoin('sex as s', 's.code', 'p.sex')
    .leftJoin('occupation as oc', 'oc.occupation', 'p.occupation')
    .where('p.hn', hn)
    .orderBy('o.vstdate', 'desc')
    .limit(1);
    return data[0];
  }

}
