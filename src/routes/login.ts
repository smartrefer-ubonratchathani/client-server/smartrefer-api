import { FastifyInstance, FastifyRequest, FastifyReply } from 'fastify'
import { z } from 'zod';

// model
import { HisUniversalHiModel } from '../models/his_universal.model';
import { HisHiModel } from '../models/his_hi.model';
import { HisHomcModel } from '../models/his_homc.model';
import { HisHosxpv3Model } from '../models/his_hosxpv3.model';
import { HisHosxpv3ArjaroModel } from '../models/his_hosxpv3arjaro.model';
import { HisHosxpv4Model } from '../models/his_hosxpv4.model';
import { HisHosxpv4PgModel } from '../models/his_hosxpv4pg.model';
import { HisMbaseModel } from '../models/his_mbase.model';
import { HisPhospModel } from '../models/his_phosp.model';
import { HisHimproHiModel } from '../models/his_himpro.model';
import { HisJhcisHiModel } from '../models/his_jhcis.model';
import { HisUbaseModel } from '../models/his_ubase.model';
import { HisSakonModel } from '../models/his_sakon.model';
import { HisHomcUbonModel } from '../models/his_homc_udon.model';
import { HisSoftConModel } from '../models/his_softcon.model'
import { HisSoftConUdonModel } from '../models/his_softcon_udon.model'
import { HisSsbModel } from '../models/his_ssb.model'
import { HisPanaceaplusModel } from '../models/his_panaceaplus.model'
import { HisEphisHiModel } from '../models/his_ephis.model';
import { HisHosxpPcuXEModel } from '../models/his_hosxppcuxe.model';
import { HisHosxpv4PCUModel } from '../models/his_hosxpv4_pcu.model';
import { HisHosxpPCUModel } from '../models/his_hosxp_pcu.model';
import { HisUbonrakThonburiModel } from '../models/his_ubonrakthonburi.model';
import { HisHosxpv3loeiModel } from '../models/his_hosxpv3loei.model';

import { RerferOutModel } from '../models/ket/referout'
import { RerferBackModel } from '../models/ket/referback'


const rerferOutModel = new RerferOutModel();
const rerferBackModel = new RerferBackModel();
// const axios = require('axios');
import axios from 'axios';

export default async function login(fastify: FastifyInstance) {

  // ห้ามแก้ไข // 
  //-----------------BEGIN START-------------------//
  const provider = process.env.HIS_PROVIDER;
  const hoscode = process.env.HIS_CODE;
  const connection = process.env.DB_CLIENT;
  let db: any;
  let hisModel: any;

  switch (connection) {
    case 'mysql2':
      db = fastify.mysql2;
      break;
    case 'mysql':
      db = fastify.mysql;
      break;
    case 'mssql':
      db = fastify.mssql;
      break;
    case 'pg':
      db = fastify.pg;
      break;
    case 'oracledb':
      db = fastify.oracledb;
      break;
    default:
      db = fastify.mysql;
  }

  switch (provider) {
    case 'ubase':
      hisModel = new HisUbaseModel();
      break;
    case 'himpro':
      hisModel = new HisHimproHiModel();
      break;
    case 'jhcis':
      hisModel = new HisJhcisHiModel();
      break;
    case 'hosxpv3':
      hisModel = new HisHosxpv3Model();
      break;
    case 'hosxpv3arjaro':
      hisModel = new HisHosxpv3ArjaroModel();
      break;
    case 'hosxpv4':
      hisModel = new HisHosxpv4Model();
      break;
    case 'hosxpv4pg':
      hisModel = new HisHosxpv4PgModel();
      break;
    case 'hi':
      hisModel = new HisHiModel();
      break;
    case 'homc':
      hisModel = new HisHomcModel();
      break;
    case 'mbase':
      hisModel = new HisMbaseModel();
      break;
    case 'phosp':
      hisModel = new HisPhospModel();
      break;
    case 'universal':
      hisModel = new HisUniversalHiModel();
      break;
    case 'sakon':
      hisModel = new HisSakonModel();
      break;
    case 'homcudon':
      hisModel = new HisHomcUbonModel();
      break;
    case 'softcon':
      hisModel = new HisSoftConModel();
      break;
    case 'softconudon':
      hisModel = new HisSoftConUdonModel();
      break;
    case 'ssb':
      hisModel = new HisSsbModel();
      break;
    case 'panaceaplus':
      hisModel = new HisPanaceaplusModel();
      break;
    case 'hosxppcuv4':
      hisModel = new HisHosxpv4PCUModel();
      break;
    case 'ephis':
      hisModel = new HisEphisHiModel();
      break;
    case 'hosxppcuxe':
      hisModel = new HisHosxpPcuXEModel();
      break;
    case 'hosxppcu':
      hisModel = new HisHosxpPCUModel();
      break;
    case 'ubonrakthonburi':
      hisModel = new HisUbonrakThonburiModel();
      break;
      case 'hosxploei':
        hisModel = new HisHosxpv3loeiModel();
        break;

    default:
    // hisModel = new HisModel();
  }
  //------------------BEGIN END------------------//


  fastify.post('/', async (request: FastifyRequest, reply: FastifyReply) => {
    try {
        // Define validation schema for request body
        const bodySchema = z.object({
            username: z.string().min(1, "Username is required"),
            password: z.string().min(1, "Password is required"),
        });

        // Validate the request body
        const body = bodySchema.parse(request.body);
        const { username, password } = body;

        console.log(username, '/', password);

        let res_login: any;

        if (process.env.HIS_PROVIDER === 'ephis') {
            try {
                const response = await axios.post(
                    `https://rhisapi.rajavithi.go.th/WS_IDRJVT/api/WS_IDRJVT?userid=${username}&pass=${password}`,
                    {},
                    { headers: { 'Content-Type': 'application/json' } }
                );

                if (response.status === 200) {
                    const cid = response.data;
                    res_login = await hisModel.getLogin(db, cid);
                } else {
                    throw new Error(`Invalid response status: ${response.status}`);
                }
            } catch (error) {
                console.error(error);
                reply.status(500).send({ ok: false, error: "Failed to authenticate with ePHIS" });
                return;
            }
        } else {
            res_login = await hisModel.getLogin(db, username, password);

            if (["homc", "homcudon", "softcon", "softconudon", "panaceaplus"].includes(process.env.HIS_PROVIDER || "")) {
                res_login = [res_login];
            }
        }

        if (res_login?.[0]) {
            if (res_login[0].username && res_login[0].hcode) {
                console.log('Generating token payload');

                const token = fastify.jwt.sign({ res_login }, { expiresIn: '1d' });

                reply.send({
                    ok: true,
                    payload: {
                        gateway_token: token,
                        info: res_login
                    }
                });
            } else {
                reply.status(401).send({ ok: false, error: "Invalid user credentials" });
            }
        } else {
            reply.status(401).send({ ok: false, error: "Authentication failed" });
        }
    } catch (error) {
        if (error instanceof z.ZodError) {
            reply.status(400).send({ ok: false, error: error.errors });
        } else {
            reply.status(500).send({ ok: false, error: "Internal Server Error" });
        }
    }
});


  fastify.post('/fuse', async (request: FastifyRequest, reply: FastifyReply) => {
    try {
        // Define validation schema for request body
        const bodySchema = z.object({
            username: z.string().min(1, "Username is required"),
            password: z.string().min(1, "Password is required"),
        });

        // Validate the request body
        const body = bodySchema.parse(request.body);
        const { username, password } = body;

        console.log(username, '/', password);

        let res_login = await hisModel.getLogin(db, username, password);

        // Check HIS_PROVIDER conditions
        if (["homc", "homcudon", "softcon", "softconudon", "panaceaplus"].includes(process.env.HIS_PROVIDER || "")) {
            res_login = [res_login];
        }

        const date = new Date();
        const iat = Math.floor(date.getTime() / 1000);
        const exp = Math.floor((date.setDate(date.getDate() + 7)) / 1000);

        if (res_login[0]) {
            if (res_login[0].username && res_login[0].hcode) {
                const user = res_login[0];

                const info = {
                    id: user.username,
                    name: user.fullname,
                    hcode: user.hcode,
                    email: 'ubondev@gmail.com',
                    avatar: 'assets/images/avatars/brian-hughes.jpg',
                    status: 'online'
                };

                const payload = {
                    iat,
                    iss: 'Fuse',
                    exp
                };

                const token = fastify.jwt.sign(payload);
                reply.send({ accessToken: token, user: info, tokenType: 'bearer' });
            } else {
                reply.status(401).send({ ok: false, error: "User not found or invalid credentials" });
            }
        } else {
            reply.status(401).send({ ok: false, error: "Authentication failed" });
        }
    } catch (error) {
        if (error instanceof z.ZodError) {
            reply.status(400).send({ ok: false, error: error.errors });
        } else {
            reply.status(500).send({ ok: false, error: "Internal Server Error" });
        }
    }
});

}
