import { FastifyInstance, FastifyRequest, FastifyReply } from 'fastify'
import { ReportQuestionModel } from '../../models/coc/report_question'

const reportQuestionModel = new ReportQuestionModel();
export default async function ReportQuestion(fastify: FastifyInstance) {

    // select
    fastify.post('/question',{ preValidation: [fastify.authenticate] },  async (request: FastifyRequest, reply: FastifyReply) => {
        const req: any = request
        const token = req.headers.authorization.split(' ')[1];
        const info:any = req.body;

        try {
            let res_: any = await reportQuestionModel.question(token,info);
            reply.send(res_);
          } catch (error) {
            reply.send({ ok: false, error: error });
          }
    })

    fastify.post('/catagory',{ preValidation: [fastify.authenticate] },  async (request: FastifyRequest, reply: FastifyReply) => {
        const req: any = request
        const token = req.headers.authorization.split(' ')[1];
        const info:any = req.body;        

        try {
            let res_: any = await reportQuestionModel.catagory(token,info);
            reply.send(res_);
          } catch (error) {
            reply.send({ ok: false, error: error });
          }
    })

    fastify.post('/select_register',{ preValidation: [fastify.authenticate] },  async (request: FastifyRequest, reply: FastifyReply) => {
      const req: any = request
      const token = req.headers.authorization.split(' ')[1];
      const info:any = req.body;        

      try {
          let res_: any = await reportQuestionModel.select_register(token,info);
          reply.send(res_);
        } catch (error) {
          reply.send({ ok: false, error: error });
        }
  })
  fastify.post('/select_process_all',{ preValidation: [fastify.authenticate] },  async (request: FastifyRequest, reply: FastifyReply) => {
    const req: any = request
    const token = req.headers.authorization.split(' ')[1];
    const info:any = req.body;        

    try {
        let res_: any = await reportQuestionModel.select_process_all(token,info);
        reply.send(res_);
      } catch (error) {
        reply.send({ ok: false, error: error });
      }
})

fastify.post('/select_coc',{ preValidation: [fastify.authenticate] },  async (request: FastifyRequest, reply: FastifyReply) => {
  const req: any = request
  const token = req.headers.authorization.split(' ')[1];
  const info:any = req.body;        

  try {
      let res_: any = await reportQuestionModel.select_coc(token,info);
      reply.send(res_);
    } catch (error) {
      reply.send({ ok: false, error: error });
    }
})

fastify.post('/select_coc_from_hcode',{ preValidation: [fastify.authenticate] },  async (request: FastifyRequest, reply: FastifyReply) => {
  const req: any = request
  const token = req.headers.authorization.split(' ')[1];
  const info:any = req.body;        

  try {
      let res_: any = await reportQuestionModel.select_coc_from_hcode(token,info);
      reply.send(res_);
    } catch (error) {
      reply.send({ ok: false, error: error });
    }
})

fastify.post('/select_send',{ preValidation: [fastify.authenticate] },  async (request: FastifyRequest, reply: FastifyReply) => {
  const req: any = request
  const token = req.headers.authorization.split(' ')[1];
  const info:any = req.body;        

  try {
      let res_: any = await reportQuestionModel.select_send(token,info);
      reply.send(res_);
    } catch (error) {
      reply.send({ ok: false, error: error });
    }
})

fastify.post('/select_visit',{ preValidation: [fastify.authenticate] },  async (request: FastifyRequest, reply: FastifyReply) => {
  const req: any = request
  const token = req.headers.authorization.split(' ')[1];
  const info:any = req.body;        

  try {
      let res_: any = await reportQuestionModel.select_visit(token,info);
      reply.send(res_);
    } catch (error) {
      reply.send({ ok: false, error: error });
    }
})

fastify.post('/select_regisOne',{ preValidation: [fastify.authenticate] },  async (request: FastifyRequest, reply: FastifyReply) => {
  const req: any = request
  const token = req.headers.authorization.split(' ')[1];
  const info:any = req.body;        

  try {
      let res_: any = await reportQuestionModel.select_regisOne(token,info);
      reply.send(res_);
    } catch (error) {
      reply.send({ ok: false, error: error });
    }
})

fastify.post('/select_notify',{ preValidation: [fastify.authenticate] },  async (request: FastifyRequest, reply: FastifyReply) => {
  const req: any = request
  const token = req.headers.authorization.split(' ')[1];
  const info:any = req.body;        

  try {
      let res_: any = await reportQuestionModel.select_notify(token,info);
      reply.send(res_);
    } catch (error) {
      reply.send({ ok: false, error: error });
    }
})

fastify.post('/select_get_plan',{ preValidation: [fastify.authenticate] },  async (request: FastifyRequest, reply: FastifyReply) => {
  const req: any = request
  const token = req.headers.authorization.split(' ')[1];
  const info:any = req.body;        

  try {
      let res_: any = await reportQuestionModel.select_get_plan(token,info);
      reply.send(res_);
    } catch (error) {
      reply.send({ ok: false, error: error });
    }
})

fastify.post('/select_get_result',{ preValidation: [fastify.authenticate] },  async (request: FastifyRequest, reply: FastifyReply) => {
  const req: any = request
  const token = req.headers.authorization.split(' ')[1];
  const info:any = req.body;        

  try {
      let res_: any = await reportQuestionModel.select_get_result(token,info);
      reply.send(res_);
    } catch (error) {
      reply.send({ ok: false, error: error });
    }
})

fastify.post('/get_result',{ preValidation: [fastify.authenticate] },  async (request: FastifyRequest, reply: FastifyReply) => {
  const req: any = request
  const token = req.headers.authorization.split(' ')[1];
  const info:any = req.body;        

  try {
      let res_: any = await reportQuestionModel.get_result(token,info);
      reply.send(res_);
    } catch (error) {
      reply.send({ ok: false, error: error });
    }
})

fastify.put('/update_referback',{ preValidation: [fastify.authenticate] },  async (request: FastifyRequest, reply: FastifyReply) => {
  const req: any = request
  const token = req.headers.authorization.split(' ')[1];
  const info:any = req.body;   

  let refer_no = req.query.refer_no

  try {
      let res_: any = await reportQuestionModel.update_referback(token,refer_no,info);
      reply.send(res_);
    } catch (error) {
      reply.send({ ok: false, error: error });
    }
})

}