import { FastifyInstance, FastifyRequest, FastifyReply } from 'fastify'
import { ReferTypetModel } from '../../models/ket/refer_type'

const referTypetModel = new ReferTypetModel();
export default async function referResult(fastify: FastifyInstance) {

    // select
    fastify.get('/select',{ preValidation: [fastify.authenticate] },  async (request: FastifyRequest, reply: FastifyReply) => {
        const req: any = request
        const token = req.headers.authorization.split(' ')[1];

        try {
            let res_: any = await referTypetModel.list(token);
            reply.send(res_);
          } catch (error) {
            reply.send({ ok: false, error: error });
          }
    })

}