import { FastifyInstance, FastifyRequest, FastifyReply } from 'fastify'
import { AttachmentModel } from '../../models/ket/attachment'

const attachmentModel = new AttachmentModel();
export default async function attachment(fastify: FastifyInstance) {

    // select/6400000
    fastify.get('/select/:refer_no',{ preValidation: [fastify.authenticate] },  async (request: FastifyRequest, reply: FastifyReply) => {
        const req: any = request
        const token = req.headers.authorization.split(' ')[1];
        let refer_no = req.params.refer_no;

        try {
          let res_: any = await attachmentModel.select(token, refer_no);
          reply.send(res_);
        } catch (error) {
            reply.send({ ok: false, error: error });
        }
    })

        // delete?id=6400000
        fastify.delete('/delete',{ preValidation: [fastify.authenticate] },  async (request: FastifyRequest, reply: FastifyReply) => {
            const req: any = request
            const token = req.headers.authorization.split(' ')[1];
            let id = req.query.id;
    
            try {
              let res_: any = await attachmentModel.delete(token, id);
              reply.send(res_);
            } catch (error) {
                reply.send({ ok: false, error: error });
            }
        })
}