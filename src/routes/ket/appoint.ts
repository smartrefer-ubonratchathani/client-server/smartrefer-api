import { FastifyInstance, FastifyRequest, FastifyReply } from 'fastify'
import { AppointModel } from '../../models/ket/appoint'

const appointModel = new AppointModel();
export default async function barthelRate(fastify: FastifyInstance) {

    // select
    fastify.get('/list', { preValidation: [fastify.authenticate] }, async (request: FastifyRequest, reply: FastifyReply) => {
        const req: any = request
        const token = req.headers.authorization.split(' ')[1];

        try {
            let res_: any = await appointModel.list(token);
            reply.send(res_);
        } catch (error) {
            reply.send({ ok: false, error: error });
        }
    })

    fastify.get('/select_id', { preValidation: [fastify.authenticate] }, async (request: FastifyRequest, reply: FastifyReply) => {
        const req: any = request
        const token = req.headers.authorization.split(' ')[1];
        let id = req.query.id
        try {
            let res_: any = await appointModel.select_id(token, id);
            reply.send(res_);
        } catch (error) {
            reply.send({ ok: false, error: error });
        }
    })

    fastify.get('/select_appointByhcode', { preValidation: [fastify.authenticate] }, async (request: FastifyRequest, reply: FastifyReply) => {
        const req: any = request
        const token = req.headers.authorization.split(' ')[1];
        let hcode = req.query.hcode
        let clinic_code = req.query.clinic_code
        let appoint_date = req.query.appoint_date
        try {
            let res_: any = await appointModel.select_appointByhcode(token, hcode, clinic_code, appoint_date);
            reply.send(res_);
        } catch (error) {
            reply.send({ ok: false, error: error });
        }
    })

    // insert
    fastify.post('/insert', { preValidation: [fastify.authenticate] }, async (request: FastifyRequest, reply: FastifyReply) => {
        const req: any = request
        const token = req.headers.authorization.split(' ')[1];

        let info = req.body.rows;
        // console.log(info);
        try {
            let res_: any = await appointModel.insert(token, info);
            reply.send(res_);
        } catch (error) {
            reply.send({ ok: false, error: error });
        }
    })

    fastify.put('/update', { preValidation: [fastify.authenticate] }, async (request: FastifyRequest, reply: FastifyReply) => {
        const req: any = request
        const token = req.headers.authorization.split(' ')[1];

        let appoint_id = req.query.appoint_id
        let info = req.body
        try {
            let res_: any = await appointModel.update(token, appoint_id, info);
            reply.send(res_);
        } catch (error) {
            reply.send({ ok: false, error: error });
        }

    })

    fastify.get('/delete', { preValidation: [fastify.authenticate] }, async (request: FastifyRequest, reply: FastifyReply) => {
        const req: any = request
        const token = req.headers.authorization.split(' ')[1];

        let appoint_id = req.query.appoint_id
        try {
            let res_: any = await appointModel.delete(token, appoint_id);
            reply.send(res_);
        } catch (error) {
            reply.send({ ok: false, error: error });
        }
    })

    fastify.get('/select_referno', { preValidation: [fastify.authenticate] }, async (request: FastifyRequest, reply: FastifyReply) => {
        const req: any = request
        const token = req.headers.authorization.split(' ')[1];
        let refer_no = req.query.refer_no
        try {
            let res_: any = await appointModel.select_referno(token, refer_no);
            reply.send(res_);
        } catch (error) {
            reply.send({ ok: false, error: error });
        }
    })

}