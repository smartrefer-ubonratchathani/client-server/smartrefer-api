import { FastifyInstance, FastifyRequest, FastifyReply } from 'fastify'
import { RerferBackModel } from '../../models/ket/referback'

const rerferBackModel = new RerferBackModel();
export default async function referback(fastify: FastifyInstance) {

    // select
    fastify.get('/selectOne/:refer_no',{ preValidation: [fastify.authenticate] },  async (request: FastifyRequest, reply: FastifyReply) => {
        const req: any = request
        const token = req.headers.authorization.split(' ')[1];

        let refer_no = req.params.refer_no;
        try {
            let res_: any = await rerferBackModel.rfback_selectOne(token, refer_no);
            reply.send(res_);
        } catch (error) {
            reply.send({ ok: false, error: error });
        }
    })

    fastify.get('/select/:hcode/:sdate/:edate/:limit',{ preValidation: [fastify.authenticate] },  async (request: FastifyRequest, reply: FastifyReply) => {
        const req: any = request
        const token = req.headers.authorization.split(' ')[1];

        let limit = req.params.limit;
        let hcode = req.params.hcode;
        let sdate = req.params.sdate;
        let edate = req.params.edate;
        try {
            let res_: any = await rerferBackModel.rfback_select(token, hcode, sdate, edate, limit);
            reply.send(res_);
        } catch (error) {
            reply.send({ ok: false, error: error });
        }
    })

    fastify.get('/selectCid/:cid/:hcode/:refer',{ preValidation: [fastify.authenticate] },  async (request: FastifyRequest, reply: FastifyReply) => {
        const req: any = request
        // let cid = req.params.cid;
        // let hcode = req.params.hcode;
        // let refer = req.params.refer;
        const token = req.headers.authorization.split(' ')[1];

        let cid = req.params.cid;
        let hcode = req.params.hcode;
        let refer = req.params.refer;

        try {
            let res_: any = await rerferBackModel.rfback_select_cid(token, cid,hcode,refer);
            reply.send(res_);
        } catch (error) {
            reply.send({ ok: false, error: error });
        }
    })


    fastify.post('/insert',{ preValidation: [fastify.authenticate] },  async (request: FastifyRequest, reply: FastifyReply) => {
        const req: any = request
        const token = req.headers.authorization.split(' ')[1];

        let info_ =  JSON.stringify(req.body.rows);
        try {
            let res_: any = await rerferBackModel.rfbackInsert(token, info_);
            reply.send({ ok: true, res_ });
        } catch (error) {
            reply.send({ ok: false, error: error });
        }
    })

    fastify.put('/update/:refer_no',{ preValidation: [fastify.authenticate] },  async (request: FastifyRequest, reply: FastifyReply) => {
        const req: any = request
        const token = req.headers.authorization.split(' ')[1];

        let refer_no = req.params.refer_no;
        let _info_ =  JSON.stringify(req.body.rows);
        try {
            let res_: any = await rerferBackModel.rfbackUpdate(token, refer_no, _info_);
            reply.send({ ok: true, res_ });
        } catch (error) {
            reply.send({ ok: false, error: error });
        }
    })

    fastify.put('/receive/delete/:refer_no',{ preValidation: [fastify.authenticate] },  async (request: FastifyRequest, reply: FastifyReply) => {
        const req: any = request
        const token = req.headers.authorization.split(' ')[1];

        let refer_no = req.params.refer_no;
        let _info_ = {
            "referback": {
                "receive_no": "0",
                "receive_date": null,
                "receive_time": null,
                "receive_spclty_id": "",
                "receive_spclty_name": "",
                "receive_refer_result_id": "",
                "receive_refer_result": "",
            }
        }
        try {
            let res_: any = await rerferBackModel.rfbackUpdate(token, refer_no, JSON.stringify(_info_));
            reply.send({ ok: true, res_ });
        } catch (error) {
            reply.send({ ok: false, error: error });
        }
    })

    fastify.put('/receive/delete_refer/:refer_no/:providerUser',{ preValidation: [fastify.authenticate] },  async (request: FastifyRequest, reply: FastifyReply) => {
        const req: any = request
        const token = req.headers.authorization.split(' ')[1];

        let refer_no = req.params.refer_no;
        let providerUser = req.params.providerUser;
        let _info_ = {
            "referback": {
                "receive_no": "0",
                "receive_date": null,
                "receive_time": null,
                "receive_spclty_id": "",
                "receive_spclty_name": "",
                "receive_refer_result_id": "",
                "receive_refer_result": "",
                "providerUser": providerUser
            }
        }
        try {
            let res_: any = await rerferBackModel.rfbackUpdate(token, refer_no, JSON.stringify(_info_));
            reply.send({ ok: true, res_ });
        } catch (error) {
            reply.send({ ok: false, error: error });
        }
    })

    fastify.get('/receive/:refer_no',{ preValidation: [fastify.authenticate] },  async (request: FastifyRequest, reply: FastifyReply) => {
        const req: any = request
        const token = req.headers.authorization.split(' ')[1];

        let refer_no = req.params.refer_no;
        try {
            let res_: any = await rerferBackModel.rfbackReceive(token, refer_no);
            reply.send(res_);
        } catch (error) {
            reply.send({ ok: false, error: error });
        }
    })

    fastify.get('/count_referback/:hcode',{ preValidation: [fastify.authenticate] },  async (request: FastifyRequest, reply: FastifyReply) => {
        const req: any = request
        const token = req.headers.authorization.split(' ')[1];

        let hcode = req.params.hcode;
        try {
            let res_: any = await rerferBackModel.count_referback(token, hcode);
            reply.send(res_);
        } catch (error) {
            reply.send({ ok: false, error: error });
        }
    })

    fastify.get('/count_referback_reply/:hcode',{ preValidation: [fastify.authenticate] },  async (request: FastifyRequest, reply: FastifyReply) => {
        const req: any = request
        const token = req.headers.authorization.split(' ')[1];

        let hcode = req.params.hcode;
        try {
            let res_: any = await rerferBackModel.count_referback_reply(token, hcode);
            reply.send(res_);
        } catch (error) {
            reply.send({ ok: false, error: error });
        }
    })

    fastify.get('/select_reply/:hcode/:sdate/:edate/:limit',{ preValidation: [fastify.authenticate] },  async (request: FastifyRequest, reply: FastifyReply) => {
        const req: any = request
        const token = req.headers.authorization.split(' ')[1];

        let limit = req.params.limit;
        let hcode = req.params.hcode;
        let sdate = req.params.sdate;
        let edate = req.params.edate;
    
        try {
            let res_: any = await rerferBackModel.rfback_select_reply(token, hcode, sdate, edate, limit);
            reply.send(res_);
        } catch (error) {
            reply.send({ ok: false, error: error });
        }
    })

    fastify.get('/count_report/:stdate/:endate/:hcode',{ preValidation: [fastify.authenticate] },  async (request: FastifyRequest, reply: FastifyReply) => {
        const req: any = request
        const token = req.headers.authorization.split(' ')[1];

        let hcode = req.params.hcode;
        let stdate = req.params.stdate;
        let endate = req.params.endate;
        try {
            let res_: any = await rerferBackModel.count_report(token, stdate, endate, hcode);
            reply.send(res_);
        } catch (error) {
            reply.send({ ok: false, error: error });
        }
    })

    fastify.get('/count_report_reply/:stdate/:endate/:hcode',{ preValidation: [fastify.authenticate] },  async (request: FastifyRequest, reply: FastifyReply) => {
        const req: any = request
        const token = req.headers.authorization.split(' ')[1];

        let hcode = req.params.hcode;
        let stdate = req.params.stdate;
        let endate = req.params.endate;
        try {
            let res_: any = await rerferBackModel.count_report_reply(token, stdate, endate, hcode);
            reply.send(res_);
        } catch (error) {
            reply.send({ ok: false, error: error });
        }
    })

    fastify.get('/delete/:refer_no',{ preValidation: [fastify.authenticate] },  async (request: FastifyRequest, reply: FastifyReply) => {
        const req: any = request
        const token = req.headers.authorization.split(' ')[1];

        let refer_no = req.params.refer_no;
        try {
            let res_: any = await rerferBackModel.del_referback(token, refer_no);
            reply.send({ ok: true, res_ });
        } catch (error) {
            reply.send({ ok: false, error: error });
        }
    })

    fastify.get('/delete_refer/:refer_no/:providerUser',{ preValidation: [fastify.authenticate] },  async (request: FastifyRequest, reply: FastifyReply) => {
        const req: any = request
        const token = req.headers.authorization.split(' ')[1];

        let refer_no = req.params.refer_no;
        let providerUser = req.params.providerUser;
        try {
            let res_: any = await rerferBackModel.del_user_referback(token, refer_no, providerUser);
            reply.send({ ok: true, res_ });
        } catch (error) {
            reply.send({ ok: false, error: error });
        }
    })

    fastify.get('/selectCID/:cid',{ preValidation: [fastify.authenticate] },  async (request: FastifyRequest, reply: FastifyReply) => {
        const req: any = request
        const token = req.headers.authorization.split(' ')[1];

        let cid = req.params.cid;
        try {
            let res_: any = await rerferBackModel.rfback_selectCID(token, cid);
            reply.send(res_);
        } catch (error) {
            reply.send({ ok: false, error: error });
        }
    })
}