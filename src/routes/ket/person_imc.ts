import { FastifyInstance, FastifyRequest, FastifyReply } from 'fastify'
import { PersonImcModel } from '../../models/ket/person_imc'

const personImcModel = new PersonImcModel();
export default async function personImc(fastify: FastifyInstance) {

    // select
    fastify.get('/select',{ preValidation: [fastify.authenticate] },  async (request: FastifyRequest, reply: FastifyReply) => {
        const req: any = request
        const token = req.headers.authorization.split(' ')[1];

        try {
            let res_: any = await personImcModel.select(token);
            reply.send(res_);
          } catch (error) {
            reply.send({ ok: false, error: error });
          }
    })

    fastify.get('/select_hcode',{ preValidation: [fastify.authenticate] },  async (request: FastifyRequest, reply: FastifyReply) => {
        const req: any = request
        const token = req.headers.authorization.split(' ')[1];
        
        const hcode = req.query.hcode
        try {
          let res_: any = await personImcModel.select_hcode(token,hcode);
          reply.send(res_);
        } catch (error) {
            reply.send({ ok: false, error: error });
        }
    })

    //select_hcode_status?hcode=xxx&status=xxx&s_date=0000-00-00&e_date=0000-00-00
    fastify.get('/select_hcode_status',{ preValidation: [fastify.authenticate] },  async (request: FastifyRequest, reply: FastifyReply) => {
      const req: any = request
      const token = req.headers.authorization.split(' ')[1];
      
      const hcode = req.query.hcode
      let status = req.query.status
      let s_date = req.query.s_date
      let e_date = req.query.e_date
    try {
        let res_: any = await personImcModel.select_hcode_status(token,hcode, status, s_date, e_date);
        reply.send(res_);
      } catch (error) {
          reply.send({ ok: false, error: error });
      }
  })


    //select_status?cid=xxx&status=xxx&s_date=0000-00-00&e_date=0000-00-00
    fastify.get('/select_status',{ preValidation: [fastify.authenticate] },  async (request: FastifyRequest, reply: FastifyReply) => {
        const req: any = request
        const token = req.headers.authorization.split(' ')[1];
        
        let cid = req.query.cid
        let status = req.query.status
        let s_date = req.query.s_date
        let e_date = req.query.e_date
        try {
          let res_: any = await personImcModel.select_status(token, cid, status, s_date, e_date);
          reply.send(res_);
        } catch (error) {
            reply.send({ ok: false, error: error });
        }
    })

    //select_cid?cid=xxx
    fastify.get('/select_cid',{ preValidation: [fastify.authenticate] },  async (request: FastifyRequest, reply: FastifyReply) => {
        const req: any = request
        const token = req.headers.authorization.split(' ')[1];
        
        let cid = req.query.cid
        try {
          let res_: any = await personImcModel.select_cid(token, cid);
          reply.send(res_);
        } catch (error) {
            reply.send({ ok: false, error: error });
        }
    })

    //person_cid?cid=xxx
    fastify.get('/person_cid',{ preValidation: [fastify.authenticate] },  async (request: FastifyRequest, reply: FastifyReply) => {
        const req: any = request
        const token = req.headers.authorization.split(' ')[1];
            
        let cid = req.query.cid
        try {
            let res_: any = await personImcModel.person_cid(token, cid);
            reply.send(res_);
        } catch (error) {
            reply.send({ ok: false, error: error });
         }
    })

    //select_register?cid=xxx
    fastify.post('/select_register',{ preValidation: [fastify.authenticate] },  async (request: FastifyRequest, reply: FastifyReply) => {
        const req: any = request
        const token = req.headers.authorization.split(' ')[1];
                
        let cid = req.query.cid
        let info = req.body.rows
        try {
          let res_: any = await personImcModel.select_register(token, cid, info);
          // console.log(res_);
          if (isNaN(res_[0])) {
            reply.send(res_);
          } else {
            reply.send(info);
          }
        } catch (error) {
            reply.send({ ok: false, error: error });
        }
     })

    fastify.post('/insert',{ preValidation: [fastify.authenticate] },  async (request: FastifyRequest, reply: FastifyReply) => {
        const req: any = request
        const token = req.headers.authorization.split(' ')[1];
                
        let info = req.body
        try {
          let res_: any = await personImcModel.insert(token, info);
          reply.send(res_);
        } catch (error) {
            reply.send({ ok: false, error: error });
        }
     })

    fastify.put('/update',{ preValidation: [fastify.authenticate] },  async (request: FastifyRequest, reply: FastifyReply) => {
        const req: any = request
        const token = req.headers.authorization.split(' ')[1];
                
        let imc_id = req.query.imc_id
        let info = req.body
        try {
          let res_: any = await personImcModel.update(token, imc_id, info);
          reply.send(res_);
        } catch (error) {
            reply.send({ ok: false, error: error });
        }
    })

    fastify.delete('/delete',{ preValidation: [fastify.authenticate] },  async (request: FastifyRequest, reply: FastifyReply) => {
        const req: any = request
        const token = req.headers.authorization.split(' ')[1];
                
        let imc_id = req.query.imc_id
        try {
          let res_: any = await personImcModel.delete(token, imc_id);
          reply.send(res_);
        } catch (error) {
            reply.send({ ok: false, error: error });
        }
    })
}