import { FastifyInstance, FastifyRequest, FastifyReply } from 'fastify'
import { StrengthModel } from '../../models/ket/strength'

const strengthModel = new StrengthModel();
export default async function strength(fastify: FastifyInstance) {

    // select
    fastify.get('/select',{ preValidation: [fastify.authenticate] },  async (request: FastifyRequest, reply: FastifyReply) => {
        const req: any = request
        const token = req.headers.authorization.split(' ')[1];

        try {
            let res_: any = await strengthModel.strength(token);
            reply.send(res_);
        } catch (error) {
            reply.send({ ok: false, error: error });
        }
    })

}