import { FastifyInstance, FastifyRequest, FastifyReply } from 'fastify'
import { EvaluateModel } from '../../models/ket/evaluate'

const evaluateModel = new EvaluateModel();
export default async function evaluate(fastify: FastifyInstance) {
    
    // select
    fastify.get('/select',{ preValidation: [fastify.authenticate] },  async (request: FastifyRequest, reply: FastifyReply) => {
        const req: any = request
        const token = req.headers.authorization.split(' ')[1];

        try {
            let res_: any = await evaluateModel.select(token);
            reply.send(res_);
          } catch (error) {
            reply.send({ ok: false, error: error });
          }
    })

    fastify.get('/select_eva_detail/:refer_no',{ preValidation: [fastify.authenticate] },  async (request: FastifyRequest, reply: FastifyReply) => {
        const req: any = request
        const token = req.headers.authorization.split(' ')[1];

        let refer_no = req.params.refer_no;
        try {
          let res_: any = await evaluateModel.select_eva_detail(token, refer_no);
          reply.send(res_);
        } catch (error) {
            reply.send({ ok: false, error: error });
        }
    })

    fastify.post('/insert_eva_detail',{ preValidation: [fastify.authenticate] },  async (request: FastifyRequest, reply: FastifyReply) => {
        const req: any = request
        const token = req.headers.authorization.split(' ')[1];

        let info_ = req.body.rows;
        try {
          let res_: any = await evaluateModel.eva_detail(token, info_);
          reply.send(res_);
        } catch (error) {
            reply.send({ ok: false, error: error });
        }     
    })

    fastify.put('/update_eva_detail/:refer_no',{ preValidation: [fastify.authenticate] },  async (request: FastifyRequest, reply: FastifyReply) => {
        const req: any = request
        const token = req.headers.authorization.split(' ')[1];

        let refer_no = req.params.refer_no;
        let info_ = req.body.rows;
        try {
          let res_: any = await evaluateModel.update_eva_detail(token, refer_no, info_);
          reply.send(res_);
        } catch (error) {
            reply.send({ ok: false, error: error });
        }    
    })

    fastify.get('/select_eva_count/:sdate/:edate/:hcode',{ preValidation: [fastify.authenticate] },  async (request: FastifyRequest, reply: FastifyReply) => {
        const req: any = request
        const token = req.headers.authorization.split(' ')[1];

        let sdate = req.params.sdate;
        let edate = req.params.edate;
        let hcode = req.params.hcode;
        try {
          let res_: any = await evaluateModel.select_eva_count(token, sdate, edate, hcode);
          reply.send(res_);
        } catch (error) {
            reply.send({ ok: false, error: error });
        }
       
    })

    fastify.get('/select_eva_show/:sdate/:edate/:hcode',{ preValidation: [fastify.authenticate] },  async (request: FastifyRequest, reply: FastifyReply) => {
        const req: any = request
        const token = req.headers.authorization.split(' ')[1];

        let sdate = req.params.sdate;
        let edate = req.params.edate;
        let hcode = req.params.hcode;
        try {
          let res_: any = await evaluateModel.select_eva_show(token, sdate, edate, hcode);
          reply.send(res_);
        } catch (error) {
            reply.send({ ok: false, error: error });
        } 
    })

    fastify.get('/select_eva_show_referin/:sdate/:edate/:hcode',{ preValidation: [fastify.authenticate] },  async (request: FastifyRequest, reply: FastifyReply) => {
        const req: any = request
        const token = req.headers.authorization.split(' ')[1];

        let sdate = req.params.sdate;
        let edate = req.params.edate;
        let hcode = req.params.hcode;
        try {
          let res_: any = await evaluateModel.select_eva_show_referin(token, sdate, edate, hcode);
          reply.send(res_);
        } catch (error) {
            reply.send({ ok: false, error: error });
        }
    })

}