import { FastifyInstance, FastifyRequest, FastifyReply } from 'fastify'
import { AppointClosedModel } from '../../models/ket/appoint_closed'

const appointClosedModel = new AppointClosedModel();
export default async function barthelRate(fastify: FastifyInstance) {

    // select
    fastify.get('/list', { preValidation: [fastify.authenticate] }, async (request: FastifyRequest, reply: FastifyReply) => {
        const req: any = request
        const token = req.headers.authorization.split(' ')[1];

        try {
            let res_: any = await appointClosedModel.list(token);
            reply.send(res_);
        } catch (error) {
            reply.send({ ok: false, error: error });
        }
    })

    fastify.get('/select_id', { preValidation: [fastify.authenticate] }, async (request: FastifyRequest, reply: FastifyReply) => {
        const req: any = request
        const token = req.headers.authorization.split(' ')[1];
        let id = req.query.id
        try {
            let res_: any = await appointClosedModel.select_id(token, id);
            reply.send(res_);
        } catch (error) {
            reply.send({ ok: false, error: error });
        }
    })

    fastify.get('/select_hcode', { preValidation: [fastify.authenticate] }, async (request: FastifyRequest, reply: FastifyReply) => {
        const req: any = request
        const token = req.headers.authorization.split(' ')[1];
        let hcode = req.query.hcode
        let clinic_code = req.query.clinic_code

        try {
            let res_: any = await appointClosedModel.select_hcode(token, hcode, clinic_code);
            reply.send(res_);
        } catch (error) {
            reply.send({ ok: false, error: error });
        }
    })

    // insert
    fastify.post('/insert', { preValidation: [fastify.authenticate] }, async (request: FastifyRequest, reply: FastifyReply) => {
        const req: any = request
        const token = req.headers.authorization.split(' ')[1];

        let info = req.body.rows;
        // console.log(info);
        try {
            let res_: any = await appointClosedModel.insert(token, info);
            reply.send(res_);
        } catch (error) {
            reply.send({ ok: false, error: error });
        }
    })

    fastify.put('/update', { preValidation: [fastify.authenticate] }, async (request: FastifyRequest, reply: FastifyReply) => {
        const req: any = request
        const token = req.headers.authorization.split(' ')[1];

        let id = req.query.id
        let info = req.body
        try {
            let res_: any = await appointClosedModel.update(token, id, info);
            reply.send(res_);
        } catch (error) {
            reply.send({ ok: false, error: error });
        }

    })

    fastify.get('/delete', { preValidation: [fastify.authenticate] }, async (request: FastifyRequest, reply: FastifyReply) => {
        const req: any = request
        const token = req.headers.authorization.split(' ')[1];

        let id = req.query.id
        try {
            let res_: any = await appointClosedModel.delete(token, id);
            reply.send(res_);
        } catch (error) {
            reply.send({ ok: false, error: error });
        }
    })

}