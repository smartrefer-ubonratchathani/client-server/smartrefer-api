import { FastifyInstance, FastifyRequest, FastifyReply } from 'fastify'
import { VerSionModel } from '../../models/ket/version'
import { BotlineModel } from '../../models/botline'
import moment from 'moment-timezone';

const verSionModel = new VerSionModel();
const botlineModel = new BotlineModel();
export default async function typept(fastify: FastifyInstance) {
    const provider = process.env.HIS_PROVIDER;
    const hoscode = process.env.HIS_CODE;
    const api_url = process.env.HIS_API_URL;

    // select
    fastify.get('/select', async (request: FastifyRequest, reply: FastifyReply) => {
        const req: any = request
        // const token = req.headers.authorization.split(' ')[1];

        try {

            let res_: any = await verSionModel.select();
            reply.send(res_);
          } catch (error) {
            let _message = `สถานบริการ : ${hoscode} / เชื่อมต่อระบบ smartrefer server : ${api_url} ไม่ได้ / ใช้ระบบ HIS PROVIDER ${provider}`;
            let lineBot = await botlineModel.botLine(_message, '0ymj7266GLi5uKrjGjA2x6jS8W7Ab4spYZQ8VADqTon');
            let info = {
              "id": 0,
              "version_web": "v0.0.0",
              "detail": `สถานบริการ ${hoscode} เชื่อมต่อระบบ smartrefer server ${api_url} ไม่ได้`,
              "d_update": "2020-01-01T17:00:00.000Z"
            }
            reply.send([info]);
          }
    })

    fastify.get('/insert/:v', async (request: FastifyRequest, reply: FastifyReply) => {
        const req: any = request
        // const token = req.headers.authorization.split(' ')[1];

        let v = req.params.v;
        let info = {
          "hcode": hoscode,
          "lastdate": moment(new Date()).tz('Asia/Bangkok').format('YYYY-MM-DD HH:mm:ss'),
          "lastversion": v,
          "apiversion": "v6.0.25671126"
        }
        try {
          let res_: any = await verSionModel.insert(info);
          reply.send(res_);
        } catch (error) {
            reply.send({ ok: false, error: error });
        }
    })
}