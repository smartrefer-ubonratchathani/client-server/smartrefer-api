import { FastifyInstance, FastifyRequest, FastifyReply } from 'fastify'
import { AppointSlotModel } from '../../models/ket/appoint_slot'

const appointSlotModel = new AppointSlotModel();
export default async function barthelRate(fastify: FastifyInstance) {

    // select
    fastify.get('/list', { preValidation: [fastify.authenticate] }, async (request: FastifyRequest, reply: FastifyReply) => {
        const req: any = request
        const token = req.headers.authorization.split(' ')[1];

        try {
            let res_: any = await appointSlotModel.list(token);
            reply.send(res_);
        } catch (error) {
            reply.send({ ok: false, error: error });
        }
    })

    fastify.get('/select_id', { preValidation: [fastify.authenticate] }, async (request: FastifyRequest, reply: FastifyReply) => {
        const req: any = request
        const token = req.headers.authorization.split(' ')[1];
        let id = req.query.id
        try {
            let res_: any = await appointSlotModel.select_id(token, id);
            reply.send(res_);
        } catch (error) {
            reply.send({ ok: false, error: error });
        }
    })

    fastify.get('/select_hcode', { preValidation: [fastify.authenticate] }, async (request: FastifyRequest, reply: FastifyReply) => {
        const req: any = request
        const token = req.headers.authorization.split(' ')[1];
        let hcode = req.query.hcode
        try {
            let res_: any = await appointSlotModel.select_hcode(token, hcode);
            reply.send(res_);
        } catch (error) {
            reply.send({ ok: false, error: error });
        }
    })

    // insert
    fastify.post('/insert', { preValidation: [fastify.authenticate] }, async (request: FastifyRequest, reply: FastifyReply) => {
        const req: any = request
        const token = req.headers.authorization.split(' ')[1];

        let info = req.body.rows;
        // console.log(info);
        try {
            let res_: any = await appointSlotModel.insert(token, info);
            reply.send(res_);
        } catch (error) {
            reply.send({ ok: false, error: error });
        }
    })

    fastify.put('/update', { preValidation: [fastify.authenticate] }, async (request: FastifyRequest, reply: FastifyReply) => {
        const req: any = request
        const token = req.headers.authorization.split(' ')[1];

        let hcode = req.query.hcode
        let clinic_code = req.query.clinic_code
        let info = req.body
        try {
            let res_: any = await appointSlotModel.update(token, hcode, clinic_code, info);
            reply.send(res_);
        } catch (error) {
            reply.send({ ok: false, error: error });
        }

    })

    fastify.get('/delete', { preValidation: [fastify.authenticate] }, async (request: FastifyRequest, reply: FastifyReply) => {
        const req: any = request
        const token = req.headers.authorization.split(' ')[1];

        let hcode = req.query.hcode
        let clinic_code = req.query.clinic_code
        try {
            let res_: any = await appointSlotModel.delete(token, hcode, clinic_code);
            reply.send(res_);
        } catch (error) {
            reply.send({ ok: false, error: error });
        }
    })



}